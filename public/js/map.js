(function ($) {
    "use strict";
    /* ==============================================
     MAP -->
     =============================================== */

    var locations = [
        ['', -20.296668, -40.300633, 2]
    ];
    try {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            scrollwheel: false,
            navigationControl: true,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true,
            styles: [{
                "stylers": [{"hue": "#000"}, {saturation: -100},
                    {gamma: 1.6}]
            }],
            center: new google.maps.LatLng(-20.296668, -40.300633),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
    } catch (e) {
        console.log(e.message)
    }

    try {
        var infowindow = new google.maps.InfoWindow();
    } catch (e) {
        console.log(e.message)
    }

    var marker, i;
    try {
        for (i = 0; i < locations.length; i++) {

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon: 'images/marker.png'
            });


            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));

        }
    } catch (e) {
        console.log(e.message)
    }

})(jQuery);