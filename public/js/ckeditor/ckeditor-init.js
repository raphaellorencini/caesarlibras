﻿function ckeditor(id) {
    CKEDITOR.replace(id, {
        filebrowserBrowseUrl: '/js/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserUploadUrl: '/js/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl: '/js/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
        height: 500,
        format_tags: 'p;h1;h2;h3;h4;h5;h6;pre;address;div',
        allowedContent: true,

        toolbarGroups: [
            {name: 'clipboard', groups: ['clipboard', 'undo']},
            {name: 'editing', groups: ['find', 'selection', 'spellchecker']},
            {name: 'links'},
            {name: 'insert', groups: ['Youtube']},
            {name: 'forms'},
            {name: 'tools'},
            {name: 'document', groups: ['mode', 'document', 'doctools']},
            {name: 'others'},
            '/',
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi']},
            {name: 'styles'},
            {name: 'colors'}
        ],
        extraPlugins: 'youtube'
    });
}