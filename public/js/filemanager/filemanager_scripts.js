function open_popup(url){
    var w = 880;
    var h = 570;
    var l = Math.floor((screen.width-w)/2);
    var t = Math.floor((screen.height-h)/2);
    var win = window.open(url, 'ResponsiveFilemanager', "scrollbars=1,width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
}

function filemanager_abrir(id){

    $('#'+id).after('<span class="input-group-btn"><a href="/js/filemanager/dialog.php?type=1&popup=1&field_id='+id+'&relative_url=1&sort_by=date&descending=1&fldr=&596a153e9dcca" class="btn btn-default filemanager-btn" style="margin-top: 26px;">Selecionar Imagem</a></span>');

    $('a.filemanager-btn').on('click',function () {
        var url = $(this).attr('href');
        open_popup(url);
        return false;
    });
}

function responsive_filemanager_callback(field_id){
    $('#'+field_id).trigger('change');
}
