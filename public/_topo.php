<?php
date_default_timezone_set('America/Sao_Paulo');
$rand = rand();
$ano_atual = new DateTime();
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="pt"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="pt"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="pt"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="pt"> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <title>Caesarlibras</title>

    <link rel="apple-touch-icon" sizes="57x57" href="images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    <link rel="manifest" href="images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css">
    <link rel="stylesheet" type="text/css" href="css/bxslider.css">
    <link rel="stylesheet" type="text/css" href="style.css?r=<?=$rand?>">

    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
</head>
<body>

<div id="loader">
    <div class="loader-container">
        <img src="images/site.gif" alt="" class="loader-site">
    </div>
</div>

<div id="wrapper">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <p>
                        <img src="images/maos_32x32.png" alt="">
                        Profissionais capacitados e certificados.
                    </p>
                </div><!-- end left -->

                <div class="col-md-6 text-right">
                    <ul class="list-inline">
                        <li>
                            <a class="social" href="#"><i class="fa fa-facebook"></i></a>
                            <a class="social" href="#"><i class="fa fa-twitter"></i></a>
                            <a class="social" href="#"><i class="fa fa-google-plus"></i></a>
                            <a class="social" href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-lock"></i> Login & Cadastro</a>
                            <div class="dropdown-menu">
                                <form method="post">
                                    <div class="form-title">
                                        <h4>Login</h4>
                                        <hr>
                                    </div>
                                    <input class="form-control" type="text" name="username" placeholder="Usuário">
                                    <div class="formpassword">
                                        <input class="form-control" type="password" name="password" placeholder="******">
                                    </div>
                                    <div class="clearfix"></div>
                                    <button type="submit" class="btn btn-block btn-primary">Entrar</button>
                                    <hr>
                                    <h4><a href="#">Cadastrar nova conta</a></h4>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div><!-- end right -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end topbar -->

    <header class="header">
        <div class="container">
            <div class="hovermenu ttmenu">
                <div class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="fa fa-bars"></span>
                        </button>
                        <div class="logo">
                            <a class="navbar-brand" href="#"><img src="images/logo.png" alt="Caesarlibras"></a>
                        </div>
                    </div><!-- end navbar-header -->

                    <div class="menu navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li class="dropdown ttmenu-half"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Quem Somos<b class="fa fa-angle-down"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="quem_somos.php">A Empresa</a></li>
                                    <li><a href="cesar.php">Cesar Cunha</a></li>
                                    <li><a href="curriculo.php">Curriculum Vitae</a></li>
                                </ul>
                            </li>

                            <li class="dropdown ttmenu-half"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Informações Gerais<b class="fa fa-angle-down"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="informacoes.php">Informações</a></li>
                                    <li><a href="#">Galeria de Fotos</a></li>
                                    <li><a href="cultura_surdos.php">Cultura dos Surdos</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Sobre</a>
                            </li>
                            <li>
                                <a href="#">Serviços</a>
                            </li>
                            <li>
                                <a href="#">Contato</a>
                            </li>
                        </ul><!-- end nav navbar-nav -->

                        <!--<ul class="nav navbar-nav navbar-right">
                            <li><a class="btn btn-primary" href="course-login.html"><i class="fa fa-sign-in"></i> Cadastrar</a></li>
                        </ul>-->

                    </div><!--/.nav-collapse -->
                </div><!-- end navbar navbar-default clearfix -->
            </div><!-- end menu 1 -->
        </div><!-- end container -->
    </header><!-- end header -->