<footer class="dark footer section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-md-6 col-xs-12">
                <div class="widget">
                    <div class="widget-title">
                        <h4>Sobre Caesarlibras</h4>
                        <hr>
                    </div>
                    <p>Etiam vehicula nibh nec magna fermentum, luctus accumsan nibh ultricies. Etiam eu sem nisi. Integer volutpat mi urna, eu lobortis nulla cursus non. Aenean placerat magna tellus, id lobortis orci lobortis non.</p>

                    <a href="#" class="btn btn-default">Leia mais</a>
                </div><!-- end widget -->
            </div><!-- end col -->

            <div class="col-md-3 col-md-6 col-xs-12">
                <div class="widget">
                    <div class="widget-title">
                        <h4>Tweets Recentes</h4>
                        <hr>
                    </div>

                    <div class="twitter-widget">
                        <ul class="latest-tweets">
                            <li>
                                <p><a href="#" title="">@Caesarlibras</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                <span>2 horas atrás</span>
                            </li>
                            <li>
                                <p><a href="#" title="">@Caesarlibras</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                <span>2 horas atrás</span>
                            </li>
                        </ul><!-- end latest-tweet -->
                    </div><!-- end twitter-widget -->

                </div><!-- end widget -->
            </div><!-- end col -->

            <div class="col-md-3 col-md-6 col-xs-12">
                <div class="widget">
                    <div class="widget-title">
                        <h4>Cursos</h4>
                        <hr>
                    </div>

                    <ul class="popular-courses">
                        <li>
                            <a href="#" title=""><img class="img-thumbnail" src="upload/service_01.png" alt=""></a>
                        </li>
                        <li>
                            <a href="#" title=""><img class="img-thumbnail" src="upload/service_02.png" alt=""></a>
                        </li>
                        <li>
                            <a href="#" title=""><img class="img-thumbnail" src="upload/service_03.png" alt=""></a>
                        </li>
                        <li>
                            <a href="#" title=""><img class="img-thumbnail" src="upload/service_04.png" alt=""></a>
                        </li>
                        <li>
                            <a href="#" title=""><img class="img-thumbnail" src="upload/service_05.png" alt=""></a>
                        </li>
                        <li>
                            <a href="#" title=""><img class="img-thumbnail" src="upload/service_06.png" alt=""></a>
                        </li>
                        <li>
                            <a href="#" title=""><img class="img-thumbnail" src="upload/service_07.png" alt=""></a>
                        </li>
                        <li>
                            <a href="#" title=""><img class="img-thumbnail" src="upload/service_08.png" alt=""></a>
                        </li>
                        <li>
                            <a href="#" title=""><img class="img-thumbnail" src="upload/service_09.png" alt=""></a>
                        </li>
                    </ul>
                </div><!-- end widget -->
            </div><!-- end col -->

            <div class="col-md-3 col-md-6 col-xs-12">
                <div class="widget">
                    <div class="widget-title">
                        <h4>Entre em contato conosco</h4>
                        <hr>
                    </div>

                    <ul class="contact-details">
                        <li><i class="fa fa-link"></i> <a href="#">www.caesarlibras.com.br</a></li>
                        <li><i class="fa fa-phone"></i> (27) 3019-4810 / 3233-3951 - FAX</li>
                        <li><i class="fa fa-fax"></i> (27) 99273-2460</li>
                        <li><i class="fa fa-home"></i> Av. Nossa Senhora da Penha, 1495 - Sala AT 504,  Ed. Corporate Center, Santa Lúcia, Vitória - ES, 29.056-905</li>
                    </ul>

                </div><!-- end widget -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</footer><!-- end section -->

<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-left">
                <p>© <?=$ano_atual->format('Y')?> Caesarlibras</p>
            </div><!-- end col -->
            <div class="col-md-6 text-right">
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->
</div><!-- end wrapper -->

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/retina.js"></script>
<script src="js/wow.js"></script>
<script src="js/carousel.js"></script>
<!-- CUSTOM PLUGINS -->
<script src="js/custom.js"></script>
<!-- SLIDER REV -->
<script src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script>
    jQuery(document).ready(function() {
        jQuery('.tp-banner').show().revolution(
            {
                dottedOverlay:"none",
                delay:16000,
                startwidth:1170,
                startheight:620,
                hideThumbs:200,
                thumbWidth:100,
                thumbHeight:50,
                thumbAmount:5,
                navigationType:"none",
                navigationArrows:"solo",
                navigationStyle:"preview3",
                touchenabled:"on",
                onHoverStop:"on",
                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,
                parallax:"mouse",
                parallaxBgFreeze:"on",
                parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
                parallaxDisableOnMobile:"off",
                keyboardNavigation:"off",
                navigationHAlign:"center",
                navigationVAlign:"bottom",
                navigationHOffset:0,
                navigationVOffset:20,
                soloArrowLeftHalign:"left",
                soloArrowLeftValign:"center",
                soloArrowLeftHOffset:20,
                soloArrowLeftVOffset:0,
                soloArrowRightHalign:"right",
                soloArrowRightValign:"center",
                soloArrowRightHOffset:20,
                soloArrowRightVOffset:0,
                shadow:0,
                fullWidth:"on",
                fullScreen:"off",
                spinner:"spinner4",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                forceFullWidth:"off",
                hideThumbsOnMobile:"off",
                hideNavDelayOnMobile:1500,
                hideBulletsOnMobile:"off",
                hideArrowsOnMobile:"off",
                hideThumbsUnderResolution:0,
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                startWithSlide:0,
                fullScreenOffsetContainer: ""
            });
    });
</script>

<!-- CUSTOM PLUGINS -->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/contact.js"></script>
<script src="js/map.js"></script>
</body>

<!-- Mirrored from templatevisual.com/demo/learnplus/index6.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 May 2016 22:35:34 GMT -->
</html>