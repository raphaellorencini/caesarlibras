<?php require '_topo.php'; ?>

        <section class="slider-section">
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500" data-thumb="images/slide01.jpg"  data-saveperformance="off"  data-title="">
                            <img src="images/slide01.jpg"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <div class="tp-caption slider_layer_01 text-center lft tp-resizeme"
                                data-x="center"
                                data-y="300"
                                data-speed="1000"
                                data-start="600"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-endspeed="1000"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><strong>Assessoria a Empresas, Associações, Orgãos Governamentais e Educacionais, <br>Profissionais Liberais, ONGs, Familiares de Surdos e Surdos</strong>
                            </div>
                            <div class="tp-caption slider_layer_02 text-center lft tp-resizeme"
                                data-x="center"
                                data-y="380"
                                data-speed="1000"
                                data-start="800"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-endspeed="1000"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Profissionais capacitados e certificados em competência no uso e ensino da língua de sinais brasileira<br>e na tradução e interpretação da língua portuguesa / libras / língua portuguesa
                            </div>
                        </li>
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500" data-thumb="images/slide02.jpg"  data-saveperformance="off"  data-title="">
                            <img src="images/slide02.jpg"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <div class="tp-caption slider_layer_03 text-center lft tp-resizeme"
                                data-x="center"
                                data-y="220" 
                                data-speed="1000"
                                data-start="600"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-endspeed="1000"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><img src="images/maos_32x32.png" alt=""> Caesarlibras
                            </div>
                            <div class="tp-caption slide_layer_bg slider_layer_04 text-center lft tp-resizeme"
                                data-x="center"
                                data-y="320" 
                                data-speed="1000"
                                data-start="800"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-endspeed="1000"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap; padding: 0 5px">Aqui terá uma idéia do Profissional Intérprete e um Instrutor Profissional de Libras
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <section class="white section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center">
                            <h4>Cursos</h4>
                            <p>Cursos de LIBRAS - Nível I, II, III</p>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->

                <div id="owl-featured" class="owl-custom">
                    <div class="owl-featured">
                        <div class="shop-item-list entry">
                            <div class="">
                                <img src="upload/course_01.png" alt="">
                                <div class="magnifier">
                                </div>
                            </div>
                            <div class="shop-item-title clearfix">
                                <h4><a href="#">Curso Nível I</a></h4>
                                <div class="shopmeta">
                                    <span class="pull-left">12 Alunos</span>
                                    <div class="rating pull-right">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div><!-- end rating -->
                                </div><!-- end shop-meta -->
                            </div><!-- end shop-item-title -->
                            <div class="visible-buttons">
                                <a title="Adicionar ao carrinho" href="#"><span class="fa fa-cart-arrow-down"></span></a>
                                <a title="Leia mais" href="#"><span class="fa fa-search"></span></a>
                            </div><!-- end buttons -->
                        </div><!-- end relative -->
                    </div><!-- end col -->

                    <div class="owl-featured">
                        <div class="shop-item-list entry">
                            <div class="">
                                <img src="upload/course_02.png" alt="">
                                <div class="magnifier">
                                </div>
                            </div>
                            <div class="shop-item-title clearfix">
                                <h4><a href="#">Curso Nível II</a></h4>
                                <div class="shopmeta">
                                    <span class="pull-left">21 Alunos</span>
                                    <div class="rating pull-right">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div><!-- end rating -->
                                </div><!-- end shop-meta -->
                            </div><!-- end shop-item-title -->
                            <div class="visible-buttons">
                                <a title="Adicionar ao carrinho" href="#"><span class="fa fa-cart-arrow-down"></span></a>
                                <a title="Leia mais" href="#"><span class="fa fa-search"></span></a>
                            </div><!-- end buttons -->
                        </div><!-- end relative -->
                    </div><!-- end col -->

                    <div class="owl-featured">
                        <div class="shop-item-list entry">
                            <div class="">
                                <img src="upload/course_03.png" alt="">
                                <div class="magnifier">
                                </div>
                            </div>
                            <div class="shop-item-title clearfix">
                                <h4><a href="#">Curso Nível III</a></h4>
                                <div class="shopmeta">
                                    <span class="pull-left">98 Alunos</span>
                                    <div class="rating pull-right">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div><!-- end rating -->
                                </div><!-- end shop-meta -->
                            </div><!-- end shop-item-title -->
                            <div class="visible-buttons">
                                <a title="Adicionar ao carrinho" href="#"><span class="fa fa-cart-arrow-down"></span></a>
                                <a title="Leia mais" href="#"><span class="fa fa-search"></span></a>
                            </div><!-- end buttons -->
                        </div><!-- end relative -->
                    </div><!-- end col -->

                    <div class="owl-featured">
                        <div class="shop-item-list entry">
                            <div class="">
                                <img src="upload/course_04.png" alt="">
                                <div class="magnifier">
                                </div>
                            </div>
                            <div class="shop-item-title clearfix">
                                <h4><a href="#">Pacote de Cursos</a></h4>
                                <div class="shopmeta">
                                    <span class="pull-left">98 Alunos</span>
                                    <div class="rating pull-right">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div><!-- end rating -->
                                </div><!-- end shop-meta -->
                            </div><!-- end shop-item-title -->
                            <div class="visible-buttons">
                                <a title="Adicionar ao carrinho" href="#"><span class="fa fa-cart-arrow-down"></span></a>
                                <a title="Leia mais" href="#"><span class="fa fa-search"></span></a>
                            </div><!-- end buttons -->
                        </div><!-- end relative -->
                    </div><!-- end col -->
                </div><!-- end owl-featured -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="grey section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center">
                            <h4>Nossa Equipe</h4>
                            <p>Conheça a equipe da Caesalibras</p>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="testimonial">
                            <img class="alignleft img-circle" src="images/func01.jpg" alt="">
                            <p>Lorem Ipsum is simply dummy text of the printing and industry. </p>
                            <div class="testimonial-meta">
                            <h4>John DOE</h4>
                            </div>
                        </div><!-- end dmbox -->
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="testimonial">
                            <img class="alignleft img-circle" src="images/func02.jpg" alt="">
                            <p>Lorem Ipsum is simply dummy text of the most popular items.</p>
                            <div class="testimonial-meta">
                            <h4>Jenny Anderson</h4>
                            </div>
                        </div><!-- end dmbox -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="testimonial">
                            <img class="alignleft img-circle" src="images/func03.jpg" alt="">
                            <p>Lorem Ipsum is simply dummy text of the printing.</p>
                            <div class="testimonial-meta">
                                <h4>Mark BOBS</h4>
                            </div>
                        </div><!-- end dmbox -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="testimonial">
                            <img class="alignleft img-circle" src="images/func04.jpg" alt="">
                            <p>Lorem Ipsum is simply dummy text of the printing.</p>
                            <div class="testimonial-meta">
                            <h4>Mark BOBS</h4>
                            </div>
                        </div><!-- end dmbox -->
                    </div><!-- end col-lg-3 -->
                </div><!-- end row -->

                <div class="button-wrapper text-center">
                    <p>Nam bibendum dapibus tortor, eget mattis odio hendrerit eget. Aliquam erat volutpat. Sed accumsan, lacus eget malesuada iaculis, dui odio aliquet nunc, ut porttitor lectus sapien sit amet nulla</p>
                </div><!-- end button-wrapper -->

            </div><!-- end container -->
        </section><!-- end section -->

        <article id="contact" class="map-section">
            <div id="map" class="wow slideInUp"></div>
        </article><!-- end section -->

<?php require '_rodape.php'; ?>