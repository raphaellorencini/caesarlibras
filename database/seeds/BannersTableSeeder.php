<?php

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->insert([
            'arquivo' => '/uploads/arquivos/slide01.jpg',
            'legenda' => '<strong>Assessoria a Empresas, Associações, Orgãos Governamentais e Educacionais, <br>Profissionais Liberais, ONGs, Familiares de Surdos e Surdos</strong>',
            'legenda2' => 'Profissionais capacitados e certificados em competência no uso e ensino da língua de sinais brasileira<br>e na tradução e interpretação da língua portuguesa / libras / língua portuguesa',
            'posicao_legenda' => 300,
            'posicao_legenda2' => 380,
            'class' => 'slider_layer_01',
            'class2' => 'slider_layer_02',
            'created_at' => new DateTime(),
        ]);

        DB::table('banners')->insert([
            'arquivo' => '/uploads/arquivos/slide02.jpg',
            'legenda' => '<img src="images/maos_32x32.png" alt="">Caesarlibras',
            'legenda2' => '&nbsp;Aqui encontrará Profissionais Tradutores-Intérpretes e Professores de Libras&nbsp;',
            'posicao_legenda' => 220,
            'posicao_legenda2' => 320,
            'class' => 'slider_layer_03',
            'class2' => 'slide_layer_bg slider_layer_04',
            'created_at' => new DateTime(),
        ]);
    }
}
