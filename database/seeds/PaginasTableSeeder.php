<?php

use Illuminate\Database\Seeder;

class PaginasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('paginas')->insert([
            'nome' => 'Home',
            'slug' => 'home',
            'created_at' => new DateTime(),
        ]);

        //2
        DB::table('paginas')->insert([
            'nome' => 'Quem Somos',
            'slug' => 'quem_somos',
            'created_at' => new DateTime(),
        ]);
        //3
        DB::table('paginas')->insert([
            'nome' => 'Cesar Cunha',
            'slug' => 'cesar',
            'created_at' => new DateTime(),
        ]);
        //4
        DB::table('paginas')->insert([
            'nome' => 'Curriculum Vitae',
            'slug' => 'curriculo',
            'created_at' => new DateTime(),
        ]);

        //5
        DB::table('paginas')->insert([
            'nome' => 'Informações Gerais',
            'slug' => 'informacoes',
            'created_at' => new DateTime(),
        ]);
        //6
        DB::table('paginas')->insert([
            'nome' => 'Cultura dos Surdos',
            'slug' => 'cultura_surdos',
            'created_at' => new DateTime(),
        ]);

        //7
        DB::table('paginas')->insert([
            'nome' => 'Sobre',
            'slug' => 'sobre',
            'created_at' => new DateTime(),
        ]);

        //8
        DB::table('paginas')->insert([
            'nome' => 'Serviços',
            'slug' => 'servicos',
            'created_at' => new DateTime(),
        ]);

        //9
        DB::table('paginas')->insert([
            'nome' => 'Contato',
            'slug' => 'contato',
            'created_at' => new DateTime(),
        ]);
    }
}
