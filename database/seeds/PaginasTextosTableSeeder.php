<?php

use Illuminate\Database\Seeder;

class PaginasTextosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Quem Somos
        DB::table('paginas_textos')->insert([
            'pagina_id' => '2',
            'nome' => 'Caesarlibras Cursos Técnicos e Gerenciais Ltda.',
            'texto' => '<p>
                            Somos uma Empresa pioneira voltada para a acessibilidade da pessoa com deficiencia promovendo através de cursos e palestras a eliminação das barreiras comunicacionais e atitudinais.
                            <br><br>

                            Nossa missão é oferecer serviços, ensino e treinamento de qualidade para o desenvolvimento profissional de surdos e ouvintes.
                            <br><br>

                            Nossa visão é ser uma Empresa de referencia no ensino, tradução e interpretação da Língua de Sinais Brasileira.
                            <br><br>

                            Nosso valor é o compromisso ético contribuindo para uma boa relação entre os seres humanos.
                            <br><br>

                            Realizamos cursos de Libras In Company, com metodologia voltada para a área específica.
                            <br><br>

                            Consultoria e Assessoria promovendo a boa relação entre gestores e colaboradores surdos.
                            <br><br>

                            Serviços de Tradução e Interpretação nas Línguas Portuguesa e Brasileira de Sinais, nas modalidades Oral e Visual e também nas modalidades escrita da Língua Portuguesa e SignWriting.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '2',
            'nome' => 'Clientes, Equipe e Serviços',
            'texto' => '<p>
                            Nas nossas atribuições como Professores e Tradutores-Intérpretes <br><br>
                            Experiência em:
                        </p>
                        <ul class="check">
                            <li>Consultoria e Assessoria em Empresas para Inclusão e Acessibilidade da Pessoa Surda e da Pessoa com Deficiência</li>
                            <li>Tradução e Interpretação em:
                                <ul>
                                    <li>Treinamentos e Integrações de colaboradores surdos nas Empresas</li>
                                    <li>Cursos de Informática e Webdesigner</li>
                                    <li>Área Educacional: Cursos de Formação, Graduação, Pós-Graduação</li>
                                    <li>Concursos e Processos Seletivos</li>
                                    <li>Peças teatrais</li>
                                    <li>Fóruns, Simpósios, Seminários, Congressos</li>
                                    <li>Poder Judiciário</li>
                                    <li>Eventos do Governo Federal, Estadual e Municipal</li>
                                </ul>
                            </li>
                        </ul>',
            'created_at' => new DateTime(),
        ]);

        //Cesar
        DB::table('paginas_textos')->insert([
            'pagina_id' => '3',
            'nome' => 'Conheça a história de César Cunha como Professor e Tradutor/Intérprete da LIBRAS',
            'texto' => '<p>
                            <a href="#"><img src="images/cesar01.jpg" alt="Cesar"></a>
                        </p>

                        <p>César Cunha no ano de 1989 no Rio de Janeiro teve contato com a comunidade surda. Os surdos logo se identificaram com ele e desejaram ensinar-lhe a Língua de Sinais.</p>
                        <p>O início foi difícil porque ele não tinha nenhuma noção do que era LIBRAS, tampouco conhecia a cultura surda. Os surdos iam em sua casa toda a segunda-feira à noite para ensinar o idioma. Um grande avanço se deu quando a comunidade surda resolveu envolver César e sua família nos passeios e festas deles. Esse convívio ajudou muito no aprendizado e contribuiu para o que ele é hoje, Mestre em Educação, Especialista em Educação Especial na Perspectiva da Inclusão, Professor, Tradutor e Intérprete de LIBRAS.</p>
                        <p>No ano de 2000 agora na cidade de Vitória, César Cunha e outros, ao realizarem um trabalho social voluntário, passaram a conhecer os problemas e dificuldades do povo surdo no Espírito Santo, passando a ter uma lista de mais de 1500 surdos moradores da Grande Vitória e interior do Estado.</p>
                        <p>César passou a visitar várias empresas para que pudessem empregar surdos mesmo com baixa escolaridade. Contou com a ajuda de amigos.</p>
                        <p>Com a Lei 10.436 de 2002, LIBRAS passou a ser a língua oficial dos surdos do Brasil.  César passou a se adequar as necessidades. Começou a procurar meios de se capacitar para o trabalho de Instrutor e Tradutor/Intérprete de LIBRAS. Já era bem conhecido pelos surdos e por ouvintes que requisitavam seus serviços.</p>
                        <p>Alcançando seus objetivos, certificou-se no Curso de Capacitação Profissional de Intérprete da LIBRAS/PORTUGUÊS (Língua de Sinais / Língua Portuguesa) realizado pelo Instituto Mãos que Falam em parceria com a Faculdade de Vila Velha - UNIVILA, com carga horária de 320 horas e também foi Certificado no Exame Nacional de Proficiência no Uso e Ensino da LIBRAS e na Tradução e Interpretação - PROLIBRAS realizado pelo MEC/INEP - UFSC (Universidade Federal de Santa Catarina), recebendo excelentes notas em seu Boletim de Desempenho.</p>
                        <p>Procurando seu desenvolvimento profissional prestou Exame Vestibular no Programa de Licenciatura e Bacharelado em LETRAS/LIBRAS pela UFSC - Universidade Federal de Santa Catarina, vindo a ser aprovado, classificado entre os primeiros lugares (6º lugar), e matriculado no Curso de Letras/Libras - Língua Brasileira de Sinais - Bacharelado.</p>
                        <p>No ano de 2007, ele e outros amigos fundaram a APILES - Associação dos Profissionais Tradutores e Intérpretes de Libras do Estado do Espírito Santo. Esta Associação em 2008 participou da criação da FEBRAPILS - Federação Brasileira dos Profissionais Tradutores e Intérpretes e Guias Intérpretes de Língua de Sinais, que filiou-se a WASLI - World Association Sign Language Interpreters.</p>
                        <p>Em 2009 funda a Caesarlibras Cursos Técnicos e Gerenciais Ltda, que passa a ser uma referência na prestação de serviços de consultoria em gestão de diversidade, ensino, tradução e interpretação na Língua Brasileira de Sinais.  Atualmente, conta com parceiros intérpretes de Libras, profissionais que juntamente com a coordenação da Empresa atendem seus clientes fixos e eventuais. A qualidade dos serviços passa a ser um referencial na área de Tradução e Interpretação na Língua Brasileira de Sinais.</p>
                        <p>No ano de 2010 é convidado pela Secretaria Estadual de Educação a Coordenar o 1º Curso Técnico de Tradução e Interpretação de Língua Brasileira de Sinais no Estado do Espírito Santo.</p>
                        <p>Em 2013, passa em concurso público e passa a atuar na Universidade Federal do Espírito Santo como tradutor e intérprete de Libras na Pró-Reitoria de Assuntos Estudantis e Cidadania, sendo nomeado para Coordenador Geral do Núcleo de Acessibilidade da referida Universidade.</p>
                        <p>No ano seguinte conclui o Curso de Educação Especial na Perspectiva da Inclusão, pela Universidade Federal do Espírito Santo.

                            No ano de 2016, a UFES lhe confere o título de Mestre em Educação, na linha de pesquisa em Diversidade e práticas educacionais inclusivas, sendo sua orientadora a Profª. Drª. Lucyenne Matos da Costa Vieira-Machado.</p>
                        <p>Hoje, César Cunha, Profissional na Área de Ensino, Tradução e Interpretação da LIBRAS, afiliado ao Sindicato Nacional de Tradutores - SINTRA, sente-se agradecido aos Surdos Adeilson (in memoriam), Adriano Zoet e outros de dentro e de fora do Estado do Espírito Santo que contribuíram para seu sucesso profissional.</p>
                        <p>César seria injusto se não mencionasse, o Intérprete Roberto que o ajudou muito em aprender o idioma e técnicas de interpretação. Ele continua tendo o mesmo sentimento e espírito de antes, quando realizava seu serviço voluntário, amar o idioma LIBRAS, seus usuários os surdos e o que faz como profissional que é.</p>',
            'created_at' => new DateTime(),
        ]);

        //Currículo
        DB::table('paginas_textos')->insert([
            'pagina_id' => '4',
            'nome' => 'Curriculum Vitae',
            'texto' => '<p>
                            Mestre em Educação na Linha de Pesquisa em Diversidade e Práticas Educacionais Inclusivas. Pós-Graduado lato sensu de Especialização em Educação Especial na Perspectiva da Inclusão. Licenciatura Plena em Língua Portuguesa, Bacharel em Tradução e Interpretação das Línguas Portuguesa e Brasileira de Sinais. Pós-graduando em Língua Brasileira de Sinais. Tradutor e Intérprete de Libras na Pró-Reitoria de Assuntos Estudantis e Cidadania, Coordenador Geral do Núcleo de Acessibilidade na Universidade Federal do Espírito Santo. Professor Bilíngue, atuou como Professor Auxiliar da Disciplina de Fundamentos da Língua Brasileira de Sinais na Universidade Federal do Espírito Santo. Tradutor e Intérprete de Libras em Shows, Peças Teatrais, Eventos e Audiências no Poder Judiciário Federal e Estadual. Experiência na área de Letras, com ênfase em Tradução e Interpretação em Língua Brasileira de Sinais e Língua Portuguesa. Consultor em Gestão de Diversidade. Coordenador e Professor em Cursos Técnicos de Formação de Tradutores e Intérpretes da Língua Brasileira de Sinais. Professor de Linguística da Língua Brasileira de Sinais; Teorias da Tradução; Técnicas de Tradução e Interpretação; Relações Históricas, Políticas e Sociais das Comunidades Surdas e de Intérpretes.
                        </p>
                        <p>
                            <a href="http://lattes.cnpq.br/6090932065520849" target="_blank"><strong>Clique aqui para acessar o currículo.</strong></a>
                        </p>
                        
                        <br>
                        <div style="text-align: center">
                            <h3>Contrate nossos Serviços com a certeza de receber <br>o melhor Ensino da Libras, Tradução e Interpretação Fidedigna e de Qualidade</h3>
                            <h3>Não contrate qualquer um que diga que sabe LIBRAS</h3>
                            <h3>Exija um Profissional TILS</h3>
                            <p><a href="'.route('site.contato').'">Contrate Agora</a></p>
                        </div>',
            'created_at' => new DateTime(),
        ]);

        //Informações
        DB::table('paginas_textos')->insert([
            'pagina_id' => '5',
            'nome' => 'Informações Importantes',
            'texto' => '<p>
                            As Línguas de Sinais (LS) são as línguas naturais das comunidades surdas. Ao contrário do que muitos imaginam, as Línguas de Sinais não são simplesmente mímicas e gestos soltos, utilizados pelos surdos para facilitar a comunicação. São línguas com estruturas gramaticais próprias.
                        </p>

                        <p>
                            Atribui-se às Línguas de Sinais o status de língua porque elas também são compostas pelos níveis lingüísticos: o fonológico, o morfológico, o sintático e o semântico. O que é denominado de palavra ou item lexical nas línguas oral-auditivas são denominados sinais nas línguas de sinais.
                        </p>

                        <p>
                            O que diferencia as Línguas de Sinais das demais línguas é a sua modalidade visual-espacial. Assim, uma pessoa que entra em contato com uma Língua de Sinais irá aprender uma outra língua, como o Francês, Inglês etc. Os seus usuários podem discutir filosofia ou política e até mesmo produzir poemas e peças teatrais.
                        </p>

                        <p>
                            LIBRAS, ou Língua Brasileira de Sinais, é a língua materna dos surdos brasileiros e, como tal, poderá ser aprendida por qualquer pessoa interessada pela comunicação com essa comunidade.
                        </p>

                        <p>
                            Como língua, esta é composta de todos os componentes pertinentes às línguas orais, como gramática semântica, pragmática sintaxe e outros elementos, preenchendo, assim, os requisitos científicos para ser considerada instrumental lingüístico de poder e força.
                        </p>

                        <p>
                            Possui todos os elementos classificatórios identificáveis de uma língua e demanda de prática para seu aprendizado, como qualquer outra língua.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '5',
            'nome' => 'Notícias e Entrevistas',
            'texto' => '<p>
                            Sueli Ramalho é surda e fala 32 línguas de sinais. Nascida em família de surdos, ela conta como superou sua deficiência.  Veja entrevista no Youtube.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '5',
            'nome' => 'Sites sobre Língua de Sinais',
            'texto' => '<p>
                            <a href="http://www.acessobrasil.org.br/libras" target="_blank"><strong>DICIONÁRIO</strong></a> LIBRAS
                        </p>
                        <p>
                            <a href="http://www.prolibras.ufsc.br/" target="_blank"><strong>PROLIBRAS</strong></a> Exame Nacional de Proficiência
                        </p>
                        <p>
                            <a href="http://www.ines.gov.br/" target="_blank"><strong>INES</strong></a> Instituto Nacional de Educação de Surdos
                        </p>
                        <p>
                            <a href="http://www.lsbvideo.com.br/" target="_blank"><strong>LSB</strong></a> Material para Divulgação da LIBRAS
                        </p>
                        <p>
                            <a href="http://www.ronice.cce.prof.ufsc.br/index.htm" target="_blank"><strong>RONICE MÜLLER</strong></a> Professora e Pesquisadora
                        </p>
                        <p>
                            <a href="http://www.congressotils.cce.ufsc.br/" target="_blank"><strong>I CONGRESSO NACIONAL DE PESQUISA EM TRADUÇÃO E INTEPRETAÇÃO DE LÍNGUA DE SINAIS BRASILEIRA</strong></a>
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '5',
            'nome' => 'Tradução Libras/Português - Desafios',
            'texto' => '<p>
                            Traduzir de sinais para voz é, provavelmente, o maior desafio para os intérpretes das línguas de sinais. A comunicação entre pessoas que falam línguas diferentes, depende de um bom tradutor.  São muitos os conhecimentos e domínios necessários para que aconteça uma boa, coerente e real tradução.
                        </p>
                        <p>
                            Atualmente, temos muitos intérpretes leitores de sinais (transliteram sinal/palavra), porém, não tradutores.  O bom tradutor deve saber que mais importante do que traduzir é traduzir para uma língua.  Daí a importância do profundo conhecimento da sua própria língua.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '5',
            'nome' => 'Três Grandes Tipos de Tradução',
            'texto' => '<p>
                            <strong>Tradução espontânea:</strong> é improvisada e que não teve nada preparado. O intérprete/tradutor não teve tempo nenhum para ver previamente o texto ou a "fala" do surdo.  Somente a experiência e a práxis facilitarão cada vez mais a atuação em determinadas situações, uma vez que não há como prever o que acontecerá. Exemplos: palestras não programadas, consultas médicas, entrevista para emprego, situações jurídicas, orientações e procedimentos em uma empresa.
                        </p>
                        <p>
                            <strong>Tradução fixa:</strong> são traduções de textos/"falas" já conhecidas, como a oração do Pai-Nosso, textos escritos conhecidos, poesias da cultura ouvinte adaptadas para a LIBRAS, textos dos Direitos Humanos, Direitos da Criança, leis, documentos oficiais, atas, peças teatrais com textos decorados.  Nessas situações, provavelmente, o tradutor/intérprete terá "in loco" o material a ser traduzido.  Detalhe: Ainda que o texto seja o mesmo, cada sinalizador colocará seu registro lingüístico personalizado, seu ritmo, sua poesia, sua personalidade, inclusive podendo ser diferente se comparado à última vez em que ele mesmo sinalizou esse mesmo texto - nunca falamos a mesma coisa da mesma forma duas vezes!
                        </p>
                        <p>
                            <strong>Tradução preparada:</strong> situação considerada ideal para uma boa tradução porque o intérprete terá condições de se preparar com antecedência.  Exemplos: Defesas de teses, apresentação de TCCs, monografias, palestras científicas, peças teatrais (os tradutores devem participar de todos os ensaios, porque serão "co-atores" durante toda a encenação) e, palestrantes de cidades diferentes das dos tradutores.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '5',
            'nome' => 'Três Grandes Mitos sobre o Tradutor/Intérprete',
            'texto' => '<div>
                            <ul class="check">
                                <li>Ouvintes que sabem LIBRAS (até em nível avançado) são bons tradutores;</li>
                                <li>Professores de Surdos, usuários da LIBRAS, são bons tradutores;</li>
                                <li>Filhos de Surdos são bons tradutores.</li>
                            </ul>
                        </div>
                        <p>
                            São três situações diferentes que requerem níveis de competência específicos nem sempre desenvolvidos por essas pessoas.  As maiores dificuldades percebidas nos Intérpretes/Tradutores  são:
                        </p>
                        <p>
                            <strong>Fisiólogicos:</strong> a própria acuidade visual do tradutor/intérprete.
                        </p>
                        <p>
                            <strong>Psicológicos:</strong> atenção, motivação, estado emocional, bloqueios psicológicos, tendência à réplica.
                        </p>
                        <p>
                            <strong>Intelectuais/cognitivos:</strong> conhecimento lingüístico, divagação, (pré)conceitos, atitude crítica.
                        </p>
                        <p>
                            <strong>Ambientais:</strong> iluminação, conforto, ausência de ruídos.
                        </p>
                        <p>
                            <strong>Metodológicos:</strong> abrangência da zona nítida, leitura seletiva, mentalização, automatismo, tempo de mirada.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '5',
            'nome' => 'A Importância do Tempo de Mirada na Tradução',
            'texto' => '<p>
                            É fundamental, para o intérprete, o uso do <strong>lag-time</strong>, esse "tempo de escutar", referido como <strong>tempo de mirada</strong>, para que se faça uma boa tradução consecutiva.  Por que é fundamental?
                        </p>
                        <p>
                            Permite que o palestrante comece seu discurso, e o intérprete inicie a sua tradução, com um <strong>tempo médio de 10 segundos</strong> de atraso, para organizar as idéias e evitar a tradução simultânea;
                        </p>
                        <p>
                            Ajuda para que <strong>não ocorra</strong> a omissão de conteúdos do discurso, por falta de entendimento ou tempo de leitura, a <strong>hipointerpretação</strong>.
                        </p>
                        <p>
                            <strong>Evita</strong> a substituição de <strong>termos errados</strong>, descontextualizados.
                        </p>
                        <p>
                            Possibilita a <strong>adaptação da língua-fonte para a língua-alvo</strong>, sem a omissão das metáforas, poesia, prosódia, etc.
                        </p>
                        <p>
                            Importante: se, por força das circunstâncias, o lag-time ficar muito longo (quando um Surdo estiver descrevendo tatilmente algum objeto grande), o silêncio demorado do tradutor poderá tirar um pouco da qualidade da tradução e incomodará os ouvintes que não estão entendendo nada. Uma sugestão, como sinal de que estamos em sintonia com o discurso, é valer-se de produções sonoras.
                        </p>
                        <p>
                            <em>Palestra "Tradução Libras-Português: Uma Questão Relacional" no V Congresso Internacional e XI Seminário Nacional do INES, de 27 a 29 de Setembro de 2006, no Rio de Janeiro, realizada por Marco Antônio Arriens, Intérprete Internacional.</em>
                        </p>',
            'created_at' => new DateTime(),
        ]);

        //Cultura de Surdos
        DB::table('paginas_textos')->insert([
            'pagina_id' => '6',
            'nome' => 'Cultura dos Surdos',
            'texto' => '<p>
                            Os surdos, além de serem indivíduos que possuem surdez, por norma são utilizadores de uma comunicação espaço-visual, como principal meio de conhecer o mundo em substituição à audição e à fala, tendo ainda uma cultura característica.
                        </p>
                        <p>
                            No Brasil eles desenvolveram a <a href="http://pt.wikipedia.org/wiki/LIBRAS" target="_blank"><span class="underline">LIBRAS</span></a>, e em Portugal, a <span class="underline">LGP</span>. Já outros, por viverem isolados ou em locais onde não exista uma comunidade surda, apenas se comunicam por gestos. Existem surdos que por imposição familiar ou opção pessoal preferem utilizar a língua falada.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '6',
            'nome' => 'Progresso na cultura surda',
            'texto' => '<p>
                            Ao longo dos anos, as pesquisas interdisciplinares sobre surdez e sobre as línguas de sinais, realizadas no Brasil e em outros países, tem contribuído para a modificação gradual da visão dos surdos, compartilhada pela sociedade ouvinte em geral.
                        </p>
                        <div>
                            Esses estudos têm classificado os surdos em duas categorias:
                            <ul>
                                <li>Os portadores de surdez patológica, normalmente adquirida em idade adulta;</li>
                                <li>E aqueles cuja surdez é um traço fisiológico distintivo, não implicando, necessariamente, em deficiência neurológica ou mental; antes, caracterizando-os como integrantes de minorias linguístico - culturais; este é o caso da maioria dos surdos congênitos.</li>
                            </ul>
                        </div>

                        <p>O fato de integrarem um grupo lingüístico-cultural distinto da maioria lingüística do seu país de origem, equipara-os a imigrantes estrangeiros. Porém, o fato de não disporem do meio de recepção da língua oral, pela audição, coloca-os em desvantagem em relação aos imigrantes, com respeito ao aprendizado e desenvolvimento da fluência nessa língua. Essa situação justifica a necessidade da mediação dos intérpretes em um número infinito de contextos e situações do quotidiano dessas pessoas.</p>
                        <p>Devido ao bloqueio auditivo, seu domínio da língua oral nunca poderá se equiparar ao domínio da sua língua materna de sinais, ainda que faça uso da leitura labial, visto que, essa técnica o habilita, quando muito, a perceber apenas os aspectos articulatórios da fonologia da língua. Daí sua enorme necessidade da mediação do intérprete de língua de sinais.</p>
                        <p>
                            No caso específico dos surdos brasileiros, cuja língua materna de sinais é a LIBRAS, os intérpretes que os assistem são chamados de <em>“Intérpretes de LIBRAS”</em>.
                        </p>
                        <div>
                            No Brasil, existem pelo menos duas situações em que a lei confere ao surdo o direito a intérprete de LIBRAS:
                            <ul>
                                <li>Nos depoimentos e julgamentos de surdos (área penal);</li>
                                <li>E no processo de inclusão de educando os surdos nas classes de ensino regular (área educacional).</li>
                            </ul>
                        </div>

                        <p>
                            Devido as constantes modificações e progresso neste campo, nas concepções de ensino de língua de sinais, atualmente, tem-se dado ênfase ao mecanismo de aprendizado visual do surdo e a sua condição bilíngüe-bicultural. Contudo, o surdo é bilíngüe-bicultural no sentido de que convive diariamente com duas línguas e culturas: sua língua materna de sinais (cultura surda) e língua oral (cultura ouvinte), ou de LIBRAS, em se tratando dos surdos brasileiros.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '6',
            'nome' => 'Deficiência auditiva',
            'texto' => '<p>
                            Termo técnico usado na área da saúde e, algumas vezes, em textos legais. refere-se a uma perda sensorial auditiva. Não designa o grupo cultural dos surdos.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '6',
            'nome' => 'Surdo-mudo',
            'texto' => '<p>
                            Provavelmente a mais antiga e incorreta denominação atribuída ao surdo. O fato de uma pessoa ser surda não significa que ela seja muda. A mudez é uma outra deficiência.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '6',
            'nome' => 'Compreendendo o mundo surdo',
            'texto' => '<p>
                            Muitas crianças surdas que se tornam adultos surdos dizem que o que mais desejavam era poder comunicar-se com os pais.
                        </p>
                        <p>
                            Por anos, muitos têm avaliado mal o conhecimento pessoal dos surdos. Alguns acham que os surdos não sabem praticamente nada, porque não ouvem nada. Há pais que super protegem seus filhos surdos ou temem integrá-los no mundo dos ouvintes. Outros encaram a língua de sinais como primitiva, ou inferior, à língua falada. Não é de admirar que, com tal ignorância, alguns surdos se sintam oprimidos e incompreendidos.
                        </p>
                        <p>
                            Todos sentem a necessidade de ser entendidos. Aparentes inabilidades podem empanar as verdadeiras habilidades e criatividades do surdo. Em contraste, muitos surdos consideram-se <em>“capacitados”</em>. Comunicam-se fluentemente entre si, desenvolvem auto-estima e têm bom desempenho acadêmico, social e espiritual. Infelizmente, os que muitos surdos sofrem, levam alguns deles a suspeitar dos ouvintes. Contudo, quando os ouvintes interessam-se sinceramente em entender a cultura surda e a língua de sinais natural, e encaram os surdos como pessoas <em>“capacitadas”</em>, todos se beneficiam.
                        </p>
                        <p>
                            A chave para uma boa comunicação com uma pessoa surda é o claro e apropriado contato visual. É uma necessidade, quando os surdos se comunicam. De fato, quando duas pessoas conversam em língua de sinais é considerado rude desviar o olhar e interromper o contato visual. E como captar a atenção de um surdo? Em vez de usar o nome da pessoa é melhor dar um leve toque no ombro ou no braço dela, acenar se a pessoa estiver perto, ou se estiver distante, fazer um sinal com a mão para outra pessoa chamar a atenção dela. Dependendo da situação, pode-se dar umas batidinhas no chão ou fazer piscar a luz. Esses e outros métodos apropriados de captar a atenção dão reconhecimento à experiência dos Surdos e fazem parte da cultura surda. Para aprender bem uma língua de sinais, precisa-se pensar nessa língua. É por isso que simplesmente aprender sinais de um dicionário de língua de sinais não seria útil em ser realmente eficiente nessa língua. Muitos aprendem diretamente com os que usam a língua de sinais no seu dia-a-dia — <em>os surdos</em>. Em todo o mundo, os surdos expandem seus horizontes usando uma rica língua de sinais.
                        </p>
                        <p>
                            <strong>Língua:</strong> Conjunto do vocabulário de um idioma, e de suas regras gramaticais; idioma. Por exemplo: inglês, português, LIBRAS.
                        </p>
                        <p>
                            <strong>Linguagem:</strong> Capacidade que o homem e alguns animais possuem para se comunicar, expressar seus pensamentos.
                        </p>
                        <p>
                            <strong>Língua de Sinais:</strong> É a língua dos surdos e que possui a sua própria estrutura e gramática através do canal comunicação visual, a língua de sinais dos surdos urbanos brasileiros é a LIBRAS, em Portugal é a LGP.
                        </p>
                        <p>
                            <strong>Cultura Surda:</strong> Ao longo dos séculos os surdos foram formando uma cultura própria centrada principalmente em sua forma de comunicação. Em quase todas as cidades do mundo vamos encontrar associações de surdos onde eles se reúnem e convivem socialmente.
                        </p>
                        <p>
                            <strong>Intérprete de Língua de Sinais:</strong> Pessoa ouvinte que interpreta para os surdos uma comunicação falada usando a língua de sinais e vice-versa.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '6',
            'nome' => 'Bebês de pais surdos:',
            'texto' => '<p>
                            “Assim como os bebês de pais ouvintes começam a balbuciar com cerca de sete meses... , os bebês de pais surdos começam a ‘balbuciar’ com as mãos imitando a língua de sinais dos pais”, mesmo sendo ouvintes.  <em>Jornal londrino The Times.</em>
                        </p>

                        <p>
                            A professora Laura Petitto, da Universidade McGill, em Montreal, Canadá, é da opinião de que os bebês nascem com sensibilidade a ritmos e padrões característicos a todos os idiomas, incluindo a língua de sinais. Ela disse que os bebês ouvintes que têm “pais surdos que sabem sinalizar, gesticulam de maneira diferente, seguindo um padrão rítmico específico, distinto de outros movimentos com as mãos... É um balbucio, mas com as mãos”. Os bebês expostos à língua de sinais produziram dois tipos de movimento com as mãos, ao passo que os que conviviam com pais ouvintes produziram apenas um tipo. Os pesquisadores usaram um sistema de rastreamento de posição para registrar os movimentos das mãos dos bebés na idade de 6, 10 e 12 meses.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '6',
            'nome' => 'Como agir diante de um surdo',
            'texto' => '<p>
                            Muitas pessoas não deficientes ficam confusas quando encontram uma pessoa com deficiência. Isso é natural. Todos podem se sentir desconfortáveis diante do "diferente". Mas esse desconforto diminui e pode até mesmo desaparecer quando existem muitas oportunidades de convivência entre pessoas deficientes e não-deficientes.
                        </p>

                        <p>
                            Ao tratar uma pessoa deficiente como se ela não tivesse uma deficiência, estaríamos ignorando uma característica muito importante dela. Dessa forma, não estaríamos nos relacionando com ela, mas com outra pessoa, que não é real.
                        </p>

                        <p>
                            A deficiência existe e é preciso levá-la na sua devida consideração. Neste sentido torna-se de grande importância não subestimar as possibilidades, nem as dificuldades e vice-versa. As pessoas com deficiência têm o direito, podem e querem tomar suas próprias decisões e assumir a responsabilidade por suas escolhas.
                        </p>
                        <p>
                            Ter uma deficiência não faz com que uma pessoa seja melhor ou pior do que uma pessoa não deficiente, ou que esta não possa ser eficiente. Provavelmente, por causa da deficiência, essa pessoa pode ter dificuldade para realizar algumas atividades, mas por outro lado, poderá ter extrema habilidade para fazer outras coisas. Exatamente como todos.
                        </p>
                        <p>
                            A maioria das pessoas com deficiência não se importa de responder perguntas, a respeito da sua deficiência e como ela realiza algumas tarefas. Quando alguém deseja alguma informação de uma pessoa deficiente, o correto seria dirigir-se diretamente a ela, e não a seus acompanhantes ou intérpretes. Segundo professores, intérpretes e os próprios surdos, ao se tomar alguns cuidados na comunicação com o surdo, confere-lhe o respeito ao qual ele tem direito.
                        </p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '6',
            'nome' => 'Algumas dicas importantes',
            'texto' => '<div>
                            <ul>
                                <li>Não é correto dizer que alguém é surdo-mudo. Muitas pessoas surdas não falam porque não aprenderam a falar. Muitas fazem a leitura labial, e podem fazer muitos sons com a garganta, ao rir, e mesmo ao gestualizar. Além disso, sua comunicação envolve todo o seu espaço, através da expressão facial-corporal, ou seja o uso da face, mãos, e braços, visto que, a forma de expressão visual-espacial é sobretudo importante em sua língua natural.</li>
                                <li>Falar de maneira clara, pronunciando bem as palavras, sem exageros, usando a velocidade normal, a não ser que ela peça para falar mais devagar.</li>
                                <li>Usar um tom normal de voz, a não ser que peçam para falar mais alto. Gritar nunca adianta.</li>
                                <li>Falar diretamente com a pessoa, não de lado ou atrás dela.</li>
                                <li>Fazer com que a boca esteja bem visível. Gesticular ou segurar algo em frente à boca torna impossível a leitura labial. Usar bigode também atrapalha.</li>
                                <li>Quando falar com uma pessoa surda, tentar ficar num lugar iluminado. Evitar ficar contra a luz (de uma janela, por exemplo), pois isso dificulta a visão do rosto.</li>
                                <li>Se souber alguma língua de sinais, tentar usá-la. Se a pessoa surda tiver dificuldade em entender, avisará. De modo geral, as tentativas são apreciadas e estimuladas.</li>
                                <li>Ser expressivo ao falar. Como as pessoas surdas não podem ouvir mudanças sutis de tom de voz, que indicam sentimentos de alegria, tristeza, sarcasmo ou seriedade, as expressões faciais, os gestos ou sinais e o movimento do corpo são excelentes indicações do que se quer dizer.</li>
                                <li>A conversar, manter sempre contato visual, se desviar o olhar, a pessoa surda pode achar que a conversa terminou.</li>
                                <li>Nem sempre a pessoa surda tem uma boa dicção. Se houver dificuldade em compreender o que ela diz, pedir para que repita. Geralmente, os surdos não se incomodam de repetir quantas vezes for preciso para que sejam entendidas.</li>
                                <li>Se for necessário, comunicar-se através de bilhetes. O importante é se comunicar. O método não é tão importante.</li>
                                <li>Quando o surdo estiver acompanhado de um intérprete, dirigir-se a ele, não ao intérprete.</li>
                                <li>Alguns preferem a comunicação escrita, alguns usam linguagem em código e outros preferem códigos próprios. Estes métodos podem ser lentos, requerem paciência e concentração.</li>
                            </ul>
                            <p>
                                Em suma, os surdos são pessoas que têm os mesmos direitos, os mesmos sentimentos, os mesmos receios, os mesmos sonhos, assim como todos. Se ocorrer alguma situação embaraçosa, uma boa dose de delicadeza, sinceridade e bom humor nunca falham.
                            </p>
                        </div>',
            'created_at' => new DateTime(),
        ]);

        //Sobre
        DB::table('paginas_textos')->insert([
            'pagina_id' => '7',
            'nome' => 'Aqui terá uma ideia do Profissional Intérprete e/ou Professor Profissional de Libras.',
            'texto' => '',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '7',
            'nome' => 'SINTRA',
            'texto' => '<p>
                            Cesar Cunha e Fernanda Nogueira são afiliados ao SINTRA - Sindicato Nacional de Tradutores.  Acessem o link
                            <a href="http://www.sintra.org.br/site/index.php?p=c&pag=tradut_02">http://www.sintra.org.br/site/index.php?p=c&pag=tradut_02</a>
                        </p>
                        <p>No Brasil, apenas 03 profissionais são reconhecidos e afiliados pelo SINTRA.</p>
                        <p>Nossa Empresa Cæsarlibras Cursos Técnicos e Gerenciais Ltda. tem Profissionais que seguem o Código de Ética e Postura Profissional dos Tradutores e Intérpretes de Libras/Português</p>
                        <p>Exija um Profissional Capacitado e Competente</p>
                        <p>Conheça Nossos Serviços</p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '7',
            'nome' => 'Código de Ética dos Intérpretes de Língua de Sinais',
            'texto' => '<p>1) O intérprete deve ser uma pessoa de alto caráter moral, honesto, consciente, confidente e de equilíbrio emocional. Ele guardará informações confidenciais e não poderá trair confidências, as quais foram confiadas à ele;</p>
                        <p>2) O intérprete deve manter uma atitude imparcial durante o transcurso da interpretação, evitando interferências e opiniões próprias, a menos que seja perguntado pelo grupo a fazê-lo.</p>
                        <p>3) O intérprete deve interpretar fielmente e com o melhor da sua habilidade, sempre transmitindo o pensamento, a intenção e o espírito do palestrante. Ele deve lembrar os limites da sua função particular - de forma neutra - e não ir além da sua responsabilidade.</p>
                        <p>4) O intérprete deve reconhecer seu próprio nível de competência e usar prudência em aceitar tarefas, procurando assistência de outros intérpretes e/ou profissionais, quando necessário, especialmente em palestras técnicas.</p>
                        <p>5) O intérprete deve adotar uma conduta adequada de se vestir, sem adereços, mantendo a dignidade da profissão e não chamando atenção indevida sobre si mesmo, durante o exercício da função;</p>
                        <p>6) O intérprete deve ser remunerado por serviços prestados e se dispor a providenciar serviços de interpretação, em situações onde fundos não são disponíveis.</p>
                        <p>7) Acordos a níveis profissionais devem ter remuneração de acordo com a tabela de cada estado, aprovada pela FENEIS;</p>
                        <p>8) O intérprete jamais deve encorajar pessoas surdas a buscarem decisões legais ou outras em seu favor;</p>
                        <p>9) O intérprete deve considerar os diversos níveis da Língua Brasileira de Sinais.</p>
                        <p>10) Em casos legais, o intérprete deve informar à autoridade quando o nível de comunicação da pessoa surda envolvida é tal, que a interpretação literal não é possível e o intérprete, então, terá de parafrasear de modo crasso o que se está dizendo para a pessoa surda e o que ela está dizendo à autoridade.</p>
                        <p>11) O intérprete deve se esforçar para reconhecer os vários tipos de assistência necessitados pelo surdo e fazer o melhor para atender as suas necessidades particulares.</p>
                        <p>12) Reconhecendo a necessidade para o seu desenvolvimento profissional, o intérprete deve se agrupar com colegas profissionais com o propósito de dividir novos conhecimentos e desenvolvimentos, procurar compreender as implicações da surdez e as necessidades particulares da pessoa surda alargando sua educação e conhecimento da vida, e desenvolver suas capacidades expressivas e receptivas em interpretação e tradução.</p>
                        <p>13) O intérprete deve procurar manter a dignidade, o respeito e a pureza da Língua de Sinais. E também deve estar pronto para aprender e aceitar sinais novos, se isto for necessário para o entendimento.</p>
                        <p>14) O intérprete deve esclarecer o público no que diz respeito ao surdo sempre que possível, reconhecendo que muitos equívocos (má informação) tem surgido por causa da falta de conhecimento do público na área da surdez e comunicação com o surdo.</p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '7',
            'nome' => 'Postura Profissional',
            'texto' => '<p>O intérprete é a pessoa em que o surdo mantém extrema confiança, tanto profissional como pessoal. Devendo ser uma pessoa íntegra e cumprir somente com o seu papel de interpretar priorizando sempre em sua prática a ética.  </p>
                        <p>O intérprete independente de seus conceitos e valores pessoais deverá sem preconceito interpretar em locais como: grupo de conscientização de homossexuais e em eventos religiosos.</p>
                        <p>O intérprete deverá manter sigilo quando for acompanhar o surdo não devendo revelar  seu nome e o local aonde foi designado para atuar.</p>
                        <p>O intérprete por ser a voz do surdo e do ouvinte deverá manter sempre sua neutralidade diante de qualquer situação.</p>
                        <p>O intérprete deverá sempre estar se aprimorando, se possível, freqüentando cursos de capacitação e outros eventos que venham colaborar para o seu aperfeiçoamento profissional e na aquisição de conhecimentos sobre a cultura surda.</p>
                        <p>O intérprete precisa ter expressão facial para que o surdo possa entender melhor a situação e, principalmente, ter postura, ou seja, não atuar de forma exagerada com o intuito de chamar a atenção.</p>
                        <p>O intérprete durante a sua atuação deverá ter intervalo de vinte em vinte minutos de revezamento com outro profissional em eventos de longa duração.</p>
                        <p>O intérprete precisa ser um profissional ético tanto com os surdos como com os seus colegas de profissão. Devendo estar sempre pronto a apoiar o próximo e estar disposto para o trabalho em equipe.</p>',
            'created_at' => new DateTime(),
        ]);

        //Serviços
        DB::table('paginas_textos')->insert([
            'pagina_id' => '8',
            'nome' => 'O que realizamos',
            'texto' => '',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '8',
            'nome' => 'Entre eles:',
            'texto' => '<ul class="check">
                            <li>Assessoria sobre como interagir com o povo surdo.  A comunidade surda tem uma cultura própria, assim como os outros povos. A maneira de enxergar o mundo é muito diferente da maneira de uma pessoa ouvinte. </li>
                            <li>Serviço de Intérprete Particular para Surdos onde Entidades, Empresas e Órgãos Públicos não disponibilizam o serviço de Tradutor/Intérprete de LIBRAS.</li>
                            <li>Realizamos Treinamentos e Palestras sobre Surdez e Cultura do Surdo para Empresas, Instituições de Ensino Públicas ou Particulares, Associações e Institutos.</li>
                            <li>Para familiares e parentes de surdos nossos serviços incluem cursos para melhor comunicação em família.</li>
                            <li>Serviços de Interpretação de Treinamentos, Cursos, Eventos, Solenidades e Entrevistas de Colaboradores nas Empresas e Outros quando da presença de um Surdo.</li>
                            <li>Serviços de Ensino/Tradução-Interpretação em Instituições de Ensino Superior.</li>
                            <li>Serviço de Ensino de Libras em aulas particulares para os que desejam aprender o idioma ou aprimorar o uso da língua de sinais e/ou prepararem-se para concursos quando a prova objetiva é disponibilizada em LIBRAS.</li>
                        </ul>

                        <p>Contrate nossos Serviços com a certeza de receber o melhor Ensino da Libras, Tradução e Interpretação Fidedigna e de Qualidade</p>
                        <p>Não contrate qualquer um que diga que sabe LIBRAS</p>
                        <p>César Cunha, primeiro tradutor/intérprete de LIBRAS no Estado do Espírto Santo afiliado ao Sindicato Nacional de Tradutores - SINTRA </p>
                        <p>Exija um Profissional TILS</p>',
            'created_at' => new DateTime(),
        ]);
        DB::table('paginas_textos')->insert([
            'pagina_id' => '8',
            'nome' => 'Sugestões para Contratação de Tradutor/Intérprete da LIBRAS',
            'texto' => '<p>O Contrato de Prestação de Serviços para Tradução/Interpretação da Libras / Português / Libras deverá ser assinado entre as partes envolvidas com pelo menos quarenta e oito horas de antecedência da realização do serviço.</p>
                        <p>Deverá ser pago aos Contratados a quantia mínima de duas horas de trabalho mesmo que todo o evento tenha uma duração menor que esta.</p>
                        <p>Não serão inclusos os valores referentes ao custo de passagens para locomoção dos contratados até o local do evento, bem como não estarão incluídos os valores referentes a alimentação e estadia quando se fizer necessário.  Estes valores serão em adição ao valor contratado para tradução/interpretação.</p>
                        <p>Para a realização de Traduções/Interpretações que tenham a duração superior a duas horas, faz-se necessário a contratação de pelo menos dois profissionais para que ocorra o revezamento de 20 em 20 minutos, para que haja repouso adequado evitando estafa mental.</p>
                        <p>O valor a ser pago pela contratação deverá contemplar o horário integral do evento para cada Tradutor/Intérprete contratado. Emitimos Nota Fiscal Eletrônica.</p>
                        <p>Ser entregue aos Tradutores/Intérpretes com antecedência hábil todo o material (escrito) referente as falas dos conferencistas do evento para que os Intérpretes possam tomar conhecimento, estudar e familiarizarem-se com o assunto a ser interpretado, tornando a tradução de melhor qualidade.  (Os Contratados se necessário assinarão termos de compromisso para manter o sigilo dos materiais)</p>
                        <p>Não será permitida a realização de filmagem do processo de Tradução/Interpretação, a não ser que estes profissionais aceitem e autorizem por escrito o uso de sua imagem.</p>
                        <p>Caso haja necessidade de prestar atendimento a pessoas surdo-cegas ou como guias-intérpretes, será feito contrato adicional para esta modalidade.</p>
                        <p>Deverá ser realizada a reserva de um espaço/local adequado (bem iluminado e sem poluição visual e auditiva) para o posicionamento dos intérpretes, a fim de que  não haja comprometimento na qualidade do serviço.  Quando da necessidade no evento de se apagarem as luzes, o Tradutor/Intérprete deverá continuar com foco de luz sobre ele.</p>
                        <p>Ficam reservados os direitos de exclusividade para execução dos serviços prestados pela Caesarlibras e sua Equipe, não podendo, por mais privilegiados que sejam outros Intérpretes não incluídos no contrato e que não são da Equipe atuarem simultaneamente.</p>
                        <p>Se o evento for em outro idioma, e o Tradutor/Intérprete dominar esse idioma e traduzir para LIBRAS, os valores contratados serão diferenciados.  Caso não, havendo tradutores para a Língua Portuguesa, o Tradutor/Intérprete deverá ter aparelhos (fones de ouvido ou orelhinhas) para que ouça a tradução para o Português e assim ele realize a Interpretação para LIBRAS.</p>
                        <p>Valores dos serviços de Tradução/Interpretação serão acordados entre as partes levando em consideração a modalidade e o nível do evento e da tradução/interpretação. </p>',
            'created_at' => new DateTime(),
        ]);

        DB::table('paginas_textos')->insert([
            'pagina_id' => '9',
            'nome' => 'Contato',
            'texto' => '
            <ul class="contact-details">
                <li><i class="fa fa-link"></i> <a href="#">www.caesarlibras.com.br</a></li>
                <li><i class="fa fa-phone"></i> (27) 3019-4810</li>
                <li><i class="fa fa-whatsapp" aria-hidden="true"></i></i> (27) 99273-2460 (Whatsapp)</li>
                <li><i class="fa fa-skype" aria-hidden="true"></i> cesar.cunha</li>
                <li><i class="fa fa-home"></i> Av. Nossa Senhora da Penha, 1495 - Sala AT 504,  Ed. Corporate Center, Santa Lúcia, Vitória - ES, 29.056-905</li>
            </ul>',
            'created_at' => new DateTime(),
        ]);
    }
}
