<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Admin',
            'email' => 'admin@admin.com.br',
            'password' => bcrypt('admin@zaq'),
            'role' => 'admin',
            'remember_token' => str_random(10),
        ]);

        factory(User::class)->create([
            'name' => 'Cesar',
            'email' => 'cesar@caesarlibras.com.br',
            'password' => bcrypt('cesar@zaq'),
            'role' => 'admin',
            'remember_token' => str_random(10),
        ]);

        factory(User::class)->create([
            'name' => 'Raphael Lorencini',
            'email' => 'raphaellorencini@gmail.com',
            'password' => bcrypt('123456'),
            'role' => 'aluno',
            'remember_token' => str_random(10),
        ]);
        factory(App\Aluno::class)->create([
            'user_id' => 3,
            'telefone' => '(27) 3327-3256',
            'celular' => '(27) 98745-3256',
            'endereco' => 'Aliquam pellentesque euismod condimentum, 1025',
            'bairro' => 'Aenean',
            'cidade' => 'Phasellus',
            'estado' => 'ES',
            'cep' => '29.100-320',
        ]);

        factory(User::class)->create([
            'name' => 'Aluno 02',
            'email' => 'aluno02@caesarlibras.com.br',
            'password' => bcrypt('123456'),
            'role' => 'aluno',
            'remember_token' => str_random(10),
        ]);
        factory(App\Aluno::class)->create([
            'user_id' => 4,
            'telefone' => '(27) 3200-3566',
            'celular' => '(27) 99882-3699',
            'endereco' => 'Nulla placerat tincidunt risus, 200',
            'bairro' => 'Nulla',
            'cidade' => 'Curabitur',
            'estado' => 'ES',
            'cep' => '29.311-520',
        ]);
    }
}
