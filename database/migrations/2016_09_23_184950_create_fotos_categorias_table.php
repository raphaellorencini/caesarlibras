<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFotosCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fotos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('grupo')->nullable();
            $table->string('nome');
            $table->text('descricao')->nullable();
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('fotos_categorias');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
