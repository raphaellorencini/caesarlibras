<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('arquivo');
            $table->string('legenda')->nullable();
            $table->string('legenda2')->nullable();
            $table->integer('posicao_legenda')->nullable()->default(300);
            $table->integer('posicao_legenda2')->nullable()->default(380);
            $table->string('class')->nullable();
            $table->string('class2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('banners');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
