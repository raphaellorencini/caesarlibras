<header class="header">
    <div class="container">
        <div class="hovermenu ttmenu">
            <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                    <div class="logo">
                        <a class="navbar-brand" href="{{route('index')}}"><img src="{{URL::asset('images/logo.png')}}" alt="Caesarlibras"></a>
                    </div>
                </div><!-- end navbar-header -->

                <div class="menu navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="dropdown ttmenu-half">
                            <a href="{{route('site.quem_somos')}}" class="dropdown-toggle">Quem Somos<b class="fa fa-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('site.quem_somos')}}">A Empresa</a></li>
                                <li><a href="{{route('site.cesar')}}">Cesar Cunha</a></li>
                                <li><a href="{{route('site.curriculo')}}">Curriculum Vitae</a></li>
                            </ul>
                        </li>

                        <li class="dropdown ttmenu-half">
                            <a href="{{route('site.informacoes')}}" class="dropdown-toggle">Informações Gerais<b class="fa fa-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('site.fotos')}}">Galeria de Fotos</a></li>
                                <li><a href="{{route('site.videos')}}">Vídeos</a></li>
                                <li><a href="{{route('site.noticias')}}">Notícias</a></li>
                                <li><a href="{{route('site.cultura_surdos')}}">Cultura dos Surdos</a></li>
                            </ul>
                        </li>
                        <li class="dropdown ttmenu-half">
                            <a href="{{route('site.sobre')}}" class="dropdown-toggle">Sobre<b class="fa fa-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('site.sobre')}}#codigo_etica">Código de Ética</a></li>
                                <li><a href="{{route('site.sobre')}}#postura_profissional">Postura Profissional</a></li>
                            </ul>
                        </li>
                        <li class="dropdown ttmenu-half">
                            <a href="{{route('site.servicos')}}" class="dropdown-toggle">Serviços<b class="fa fa-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('site.servicos')}}#contratacao">Contratação</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{route('site.contato')}}">Contato</a>
                        </li>
                    </ul><!-- end nav navbar-nav -->

                    <!--<ul class="nav navbar-nav navbar-right">
                        <li><a class="btn btn-primary" href="course-login.html"><i class="fa fa-sign-in"></i> Cadastrar</a></li>
                    </ul>-->

                </div><!--/.nav-collapse -->
            </div><!-- end navbar navbar-default clearfix -->
        </div><!-- end menu 1 -->
    </div><!-- end container -->
</header><!-- end header -->