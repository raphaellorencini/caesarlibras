@extends('admin')

@section('content')

    <div class="container">
        <h3>Nova Galeria</h3>

        <br>
        <a href="{{ route('admin.fotos_categorias.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.fotos_categorias.salvar', 'class' => 'form']) !!}

        @include('admin.fotos_categorias.form')

        {!! Form::close() !!}

    </div>

@endsection