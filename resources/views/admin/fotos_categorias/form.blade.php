{!! Form::token() !!}
<div class="form-group">
    {!! Form::label('nome','Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group input-group">
    {!! Form::label('arquivo','Imagem:') !!}
    {!! Form::text('arquivo', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao','Descrição:') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('publicado','Publicado:') !!}
    {!! Form::select('publicado', ['1' => 'Sim','2' => 'Não'], null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
</div>

@section('scripts')
    @include('includes.filemanager')
    <script>
        $(function(){
            filemanager_abrir('arquivo');
        });
    </script>
    @include('includes.validate')
@endsection