@extends('admin')

@section('content')

<div class="container">
    <h3>Galeria de Fotos</h3>

    <br>
    <a href="{{ route('admin.fotos_categorias.novo') }}" class="btn btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nova Galeria</a>
    <form action="{{route('admin.fotos_categorias.index')}}" method="get" class="form-inline" role="form" style="display:inline-block;">
        <div class="form-group">
            <label class="sr-only" for="">Buscar Galeria</label>
            <input type="text" name="busca" id="busca" class="form-control" placeholder="Buscar Galeria">
        </div>
        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> buscar</button>
    </form>
    <br><br>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Nome</th>
            <th width="20%">Imagem</th>
            <th width="10%">Publicado</th>
            <th width="20%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($fotos_categorias as $v)
        <tr>
            <td>{{$v->nome}}</td>
            <td>
                @if(!empty($v->arquivo))
                    {!! Thumb::img($v->arquivo) !!}
                @else
                    <img src="{{URL::asset('img/img-vazio.png')}}" alt="" class="img-responsive" style="width: 200px;">
                @endif
            </td>
            <td>{{Status::label_publicado($v->publicado)}}</td>
            <td>
                <a href="{{ route('admin.fotos.index',['id' => $v->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-camera" aria-hidden="true"></i> fotos</a>
                <a href="{{ route('admin.fotos_categorias.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> editar</a>
                <a href="{{ route('admin.fotos_categorias.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash" aria-hidden="true"></i> excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $fotos_categorias->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection