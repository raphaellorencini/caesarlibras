@extends('admin')

@section('content')

    <div class="container">
        <h3>Editar Galeria: {{ $foto_categoria->nome }}</h3>

        <br>
        <a href="{{ route('admin.fotos_categorias.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($foto_categoria, ['route' => ['admin.fotos_categorias.atualizar', $foto_categoria->id], 'class' => 'form']) !!}

        @include('admin.fotos_categorias.form')

        {!! Form::close() !!}

    </div>

@endsection