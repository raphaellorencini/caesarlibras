{!! Form::token() !!}

<div class="row">
    <div class="col-lg-6">
        <div class="form-group {{$botoes['css_class']}}">
            {!! Form::label('arquivo',$botoes['titulo2'].':') !!}
            {!! Form::text('arquivo', null, ['name' => 'arquivo','class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            {!! Form::label('legenda','Legenda :') !!}
            {!! Form::text('legenda', null, ['name' => 'legenda','class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
</div>

@section('scripts')
    @if($botoes['titulo'] != 'video')
        @include('includes.filemanager')
        <script>
            $(function(){
                filemanager_abrir('arquivo');
            });
        </script>
    @endif
    @include('includes.validate')
@endsection