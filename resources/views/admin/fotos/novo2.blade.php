@extends('admin')

@section('content')

    <div class="container">
        <h3>@if(!empty($botoes['titulo']) && strpos($botoes['titulo'],'Fotos') !== false) Fotos - @endif<strong>{{$foto_categoria->nome}}</strong></h3>

        <br>
        <a href="{{ route('admin.fotos.index',['foto_categoria_id' => $foto_categoria_id]) }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => ['admin.fotos.salvar2', $foto_categoria_id], 'class' => 'form']) !!}

        @include('admin.fotos.form')

        {!! Form::close() !!}

    </div>

@endsection