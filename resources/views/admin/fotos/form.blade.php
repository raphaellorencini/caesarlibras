{!! Form::token() !!}
@for($i = 0; $i < 10; $i++)
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group {{$botoes['css_class']}}">
                {!! Form::label('arquivo'.$i,$botoes['titulo2'].' '.($i+1).':') !!}
                {!! Form::text('arquivo'.$i, null, ['name' => 'foto['.$i.'][arquivo]','class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::label('legenda'.$i,'Legenda '.($i+1).':') !!}
                {!! Form::text('legenda'.$i, null, ['name' => 'foto['.$i.'][legenda]','class' => 'form-control']) !!}
            </div>
        </div>
    </div>
<hr>
@endfor

<div class="form-group">
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
</div>

@section('scripts')
    @if($botoes['titulo'] != 'video')
        @include('includes.filemanager')
        <script>
            $(function(){
                @for($i = 0; $i < 10; $i++)
                filemanager_abrir('arquivo{{$i}}');
                @endfor
            });
        </script>
    @endif
    @include('includes.validate')
@endsection