@extends('admin')

@section('content')

<div class="container">
    <h3>@if(!empty($botoes['titulo']) && strpos($botoes['titulo'],'Fotos') !== false) Fotos - @endif<strong>{{$foto_categoria->nome}}</strong></h3>

    <br>
    <a href="{{ route('admin.fotos.novo2',['id' => $foto_categoria_id]) }}" class="btn btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{$botoes['novo']}}</a>
    @if(!empty($botoes['voltar']))
    <a href="{{ route('admin.fotos_categorias.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> {{$botoes['voltar']}}</a>
    @endif
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>{{$botoes['titulo2']}}</th>
            <th>Legenda</th>
            <th width="15%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($fotos as $v)
        <tr>
            <td>
                @if(strpos($v->arquivo,'youtube') !== false || strpos($v->arquivo,'youtu.be') !== false)
                    {!! Youtube::thumb($v->arquivo,'mq') !!}
                @elseif(strpos($v->arquivo,'vimeo') !== false)
                    {!! Vimeo::thumb($v->arquivo) !!}
                @else
                    {!! Thumb::img($v->arquivo) !!}
                @endif
            </td>
            <td>{{$v->legenda}}</td>
            <td>
                <a href="{{ route('admin.fotos.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> editar</a>
                <a href="{{ route('admin.fotos.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash" aria-hidden="true"></i> excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $fotos->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection