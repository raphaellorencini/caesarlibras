{!! Form::token() !!}
<div class="form-group">
    {!! Form::label('titulo','Título:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    {!! Form::label('slug','URL:') !!}
    <div class="input-group">
        <span class="input-group-addon">http://caesarlibras.com.br/noticia/</span>
        {!! Form::text('slug', null, ['class' => 'form-control required']) !!}
    </div>
</div>

<div class="form-group input-group">
    {!! Form::label('arquivo','Imagem:') !!}
    {!! Form::text('arquivo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto','Texto:') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('publicado','Publicado:') !!}
    {!! Form::select('publicado', ['1' => 'Sim','2' => 'Não'], null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
</div>

@section('scripts')
    <script src="{{URL::asset('js/slugify.js')}}"></script>
    <script src="{{URL::asset('js/remove_acentos.js')}}"></script>
    @include('includes.filemanager')
    @include('includes.ckeditor',['ckeditor_id' => 'texto'])
    <script>
        $(function(){
            $('#titulo').change(function(){
                var valor = $(this).val();
                $('#slug').val(slugify(remove_acentos(valor)));
            });

            filemanager_abrir('arquivo');
        });
    </script>
    @include('includes.validate')
@endsection