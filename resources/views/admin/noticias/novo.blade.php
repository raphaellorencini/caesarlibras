@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Notícia</h3>

        <br>
        <a href="{{ route('admin.noticias.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.noticias.salvar', 'class' => 'form']) !!}

        @include('admin.noticias.form')

        {!! Form::close() !!}

    </div>

@endsection