@extends('admin')

@section('content')

<div class="container">
    <h3>Notícias</h3>

    <br>
    <a href="{{ route('admin.noticias.novo') }}" class="btn btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Novo Notícia</a>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Título</th>
            <th>Imagem</th>
            <th>Data</th>
            <th>Publicado</th>
            <th width="15%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($noticias as $v)
        <tr>
            <td>{{$v->titulo}}</td>
            <td>
                @if(!empty($v->arquivo))
                    {!! Thumb::img($v->arquivo) !!}
                @else
                    <img src="{{URL::asset('img/img-vazio.png')}}" alt="" class="img-responsive" style="width: 200px;">
                @endif
            </td>
            <td>{{Formata::date($v->created_at)}}</td>
            <td>{{Status::label_publicado($v->publicado)}}</td>
            <td>
                <a href="{{ route('admin.noticias.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> editar</a>
                <a href="{{ route('admin.noticias.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash" aria-hidden="true"></i> excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $noticias->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection