@extends('admin')

@section('content')

    <div class="container">
        <h3><strong>{{$pagina->nome}}</strong> - Textos</h3>

        <br>
        <a href="{{ route('admin.paginas_textos.index',['pagina_id' => $pagina_id]) }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($pagina_texto, ['route' => ['admin.paginas_textos.atualizar', $pagina_texto->id], 'class' => 'form']) !!}

        @include('admin.paginas_textos.form')

        {!! Form::close() !!}

    </div>

@endsection