@if($tipo == 'novo' || $tipo == 'editar')
<div class="form-group">
    {!! Form::label('name','Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    {!! Form::label('email','E-mail:') !!}
    {!! Form::text('email', null, ['class' => 'form-control required email']) !!}
</div>
@endif

@if($tipo == 'novo' || $tipo == 'editar_senha')
<div class="form-group">
    {!! Form::label('password','Senha:') !!}
    {!! Form::password('password', ['class' => 'form-control senha']) !!}
</div>

<div class="form-group">
    {!! Form::label('password2','Repetir Senha:') !!}
    {!! Form::password('password2', ['class' => 'form-control repetir_senha']) !!}
</div>
@endif

<div class="form-group">
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
</div>

@section('scripts')
    @include('includes.validate')

    <script>
    @if($tipo == 'editar')
        $(function(){
            $('#email').attr('readonly','readonly');
        });
    @endif
    </script>
@endsection