@extends('admin')

@section('content')

    <div class="container">
        <h3>Editar Usuário: {{ $user->name }}</h3>

        <br>
        <a href="{{ route('admin.users.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($user, ['route' => ['admin.users.atualizar', $user->id], 'class' => 'form']) !!}

        @include('admin.users.form',['tipo' => 'editar'])

        {!! Form::close() !!}

    </div>

@endsection