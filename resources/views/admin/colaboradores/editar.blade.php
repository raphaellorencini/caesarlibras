@extends('admin')

@section('content')

    <div class="container">
        <h3>Editar Colaborador: {{ $colaborador->nome }}</h3>

        <br>
        <a href="{{ route('admin.colaboradores.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($colaborador, ['route' => ['admin.colaboradores.atualizar', $colaborador->id], 'class' => 'form']) !!}

        @include('admin.colaboradores.form')

        {!! Form::close() !!}

    </div>

@endsection