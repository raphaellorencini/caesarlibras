@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Colaborador</h3>

        <br>
        <a href="{{ route('admin.colaboradores.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.colaboradores.salvar', 'class' => 'form']) !!}

        @include('admin.colaboradores.form')

        {!! Form::close() !!}

    </div>

@endsection