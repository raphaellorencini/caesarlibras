{!! Form::token() !!}

<div class="form-group">
    {!! Form::label('nome','Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group input-group">
    {!! Form::label('arquivo','Imagem (Tamanho 300 x 300px):',['title' => 'Favor usar imagens com o tamanho máximo de 300 x 300px']) !!}
    {!! Form::text('arquivo', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto','Texto:') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
</div>

@section('scripts')
    @include('includes.filemanager')
    @include('includes.ckeditor',['ckeditor_id' => 'texto'])
    <script>
        $(function(){
            filemanager_abrir('arquivo');
        });
    </script>
    @include('includes.validate')
@endsection