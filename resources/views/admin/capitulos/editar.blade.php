@extends('admin')

@section('content')

    <div class="container">
        <h3><strong>{{$curso->nome}}</strong> - Textos</h3>

        <br>
        <a href="{{ route('admin.capitulos.index',['curso_id' => $curso_id]) }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($capitulo, ['route' => ['admin.capitulos.atualizar', $capitulo->id], 'class' => 'form']) !!}

        @include('admin.capitulos.form')

        {!! Form::close() !!}

    </div>

@endsection