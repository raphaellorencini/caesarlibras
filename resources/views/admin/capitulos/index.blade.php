@extends('admin')

@section('content')

<div class="container">
    <h3><strong>{{$curso->nome}}</strong> - Capítulos</h3>

    <br>
    <a href="{{ route('admin.capitulos.novo',['curso_id' => $curso->id]) }}" class="btn btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Novos Capítulos</a>
    <a href="{{ route('admin.cursos.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Voltar para Cursos</a>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Capítulo</th>
            <th>Texto</th>
            <th width="20%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($capitulos as $v)
        <tr>
            <td>{{$v->nome}}</td>
            <td>{{substr(strip_tags($v->texto),0,30)}}...</td>
            <td>
                <a href="{{ route('admin.aulas.index',['capitulo_id' => $v->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-video-camera" aria-hidden="true"></i> aulas</a>
                <a href="{{ route('admin.capitulos.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> editar</a>
                <a href="{{ route('admin.capitulos.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash" aria-hidden="true"></i> excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $capitulos->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection