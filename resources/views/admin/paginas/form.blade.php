{!! Form::token() !!}
<div class="form-group">
    {!! Form::label('nome','Página:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group" style="display: none;">
    {!! Form::label('slug','URL:') !!}
    <div class="input-group">
        <span class="input-group-addon">http://gatilneway.com.br/artigo/</span>
        {!! Form::text('slug', null, ['class' => 'form-control required']) !!}
    </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
</div>
@section('scripts')
    <script src="{{URL::asset('js/slugify.js')}}"></script>
    <script src="{{URL::asset('js/remove_acentos.js')}}"></script>
    @include('includes.filemanager')
    <script>
        $(function(){
            $('#nome').change(function(){
                var valor = $(this).val();
                $('#slug').val(slugify(remove_acentos(valor)));
            });
        });
    </script>
    @include('includes.validate')
@endsection