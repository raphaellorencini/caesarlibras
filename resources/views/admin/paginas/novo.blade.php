@extends('admin')

@section('content')

    <div class="container">
        <h3>Nova Página</h3>

        <br>
        <a href="{{ route('admin.paginas.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.paginas.salvar', 'class' => 'form']) !!}

        @include('admin.paginas.form')

        {!! Form::close() !!}

    </div>

@endsection

