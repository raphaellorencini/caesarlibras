@extends('admin')

@section('content')

    <div class="container">
        <h3>Editar Curso:</h3>

        <br>
        <a href="{{ route('admin.cursos.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($curso, ['route' => ['admin.cursos.atualizar', $curso->id], 'class' => 'form']) !!}

        @include('admin.cursos.form')

        {!! Form::close() !!}

    </div>
@endsection