{!! Form::token() !!}
<div class="form-group">
    {!! Form::label('nome','Curso:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group input-group">
    {!! Form::label('arquivo','Imagem (Tamanho 300 x 300px):',['title' => 'Favor usar imagens com o tamanho máximo de 300 x 300px']) !!}
    {!! Form::text('arquivo', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    {!! Form::label('preco','Preço:') !!}
    {!! Form::text('preco', null, ['class' => 'form-control required currency']) !!}
</div>

<div class="form-group">
    {!! Form::label('desconto','Desconto:') !!}
    <div class="input-group">
        <span class="input-group-addon">%</span>
        {!! Form::text('desconto', null, ['class' => 'form-control percent']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('texto','Texto:') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('publicado','Publicado:') !!}
    {!! Form::select('publicado', ['1' => 'Sim','2' => 'Não'], null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
</div>
@section('scripts')
    @include('includes.ckeditor',['ckeditor_id' => 'texto'])
    @include('includes.filemanager')
    @include('includes.validate')
    <script src="{{ URL::asset('js/autonumeric/autoNumeric-min.js') }}"></script>
    <script>
        $(function(){
            filemanager_abrir('arquivo');
            $('input.currency').autoNumeric("init",{
                aSep: '.',
                aDec: ',',
                aSign: ''
            });
            $('input.percent').autoNumeric("init",{
                vMin: '0',
                vMax: '999999999',
                aSep: '',
                aDec: ',',
                aSign: ''
            });
        });
    </script>
@endsection