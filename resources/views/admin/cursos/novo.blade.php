@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Curso</h3>

        <br>
        <a href="{{ route('admin.cursos.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.cursos.salvar', 'class' => 'form']) !!}

        @include('admin.cursos.form')

        {!! Form::close() !!}

    </div>

@endsection

