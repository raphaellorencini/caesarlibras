@extends('admin')

@section('content')

<div class="container">
    <h3>Cursos</h3>

    <br>
    <div class="row">
        <div class="col-lg-2">
            <a href="{{ route('admin.cursos.novo') }}" class="btn btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Novo Curso</a>
        </div>
        <div class="col-lg-10">
            <form class="form-inline" action="{{ route('admin.cursos.index') }}" method="get">
                <div class="form-group">
                    <label for="">Filtro de busca</label>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="busca" id="busca" value="{{!empty($params['busca']) ? $params['busca'] : null}}">
                </div>
                <div class="form-group">
                    {!! Form::select('publicado', ['' => 'Publicados','1' => 'Sim','2' => 'Não'], !empty($params['publicado']) ? $params['publicado'] : null, ['class' => 'form-control required']) !!}
                </div>
                <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                <a href="{{ route('admin.cursos.index') }}" class="btn btn-default"><i class="fa fa-times-circle" aria-hidden="true"></i> Limpar</a>
            </form>
        </div>
    </div>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Imagem</th>
            <th>Curso</th>
            <th width="20%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cursos as $v)
        <tr>
            <td width="205">{!! Thumb::img($v->arquivo,200) !!}</td>
            <td>{{$v->nome}}</td>
            <td>
                <a href="{{ route('admin.capitulos.index',['curso_id' => $v->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-file-text-o" aria-hidden="true"></i> capítulos</a>
                <a href="{{ route('admin.cursos.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> editar</a>
                <a href="{{ route('admin.cursos.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash" aria-hidden="true"></i> excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $cursos->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection