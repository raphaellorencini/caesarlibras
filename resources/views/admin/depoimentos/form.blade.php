{!! Form::token() !!}

<div class="form-group">
    {!! Form::label('nome','Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto','Texto:') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
</div>

@section('scripts')
    @include('includes.validate')
@endsection