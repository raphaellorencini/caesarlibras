@extends('admin')

@section('content')

<div class="container">
    <h3>Depoimentos</h3>

    <br>
    <a href="{{ route('admin.depoimentos.novo') }}" class="btn btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Novo Depoimento</a>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th width="25%">Nome</th>
            <th>Texto</th>
            <th width="10%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($depoimentos as $v)
        <tr>
            <td>{{$v->nome}}</td>
            <td>{{substr($v->texto,0,100)}}...</td>
            <td>
                <a href="{{ route('admin.depoimentos.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> editar</a>
                <a href="{{ route('admin.depoimentos.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash" aria-hidden="true"></i> excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $depoimentos->render() !!}
</div>

@endsection
@section('scripts')
    @include('includes.delete_alert')
@endsection