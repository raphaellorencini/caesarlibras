@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Depoimento</h3>

        <br>
        <a href="{{ route('admin.depoimentos.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.depoimentos.salvar', 'class' => 'form']) !!}

        @include('admin.depoimentos.form')

        {!! Form::close() !!}

    </div>

@endsection