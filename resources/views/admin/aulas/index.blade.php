@extends('admin')

@section('content')

<div class="container">
    <h3><strong>{{$curso->nome}} / {{$capitulo->nome}}</strong> - Aulas</h3>

    <br>
    <a href="{{ route('admin.aulas.novo',['capitulo_id' => $capitulo->id]) }}" class="btn btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Novas Aulas</a>
    <a href="{{ route('admin.capitulos.index',['curso_id' => $capitulo->curso_id]) }}" class="btn btn-default"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Voltar para Capítulos</a>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th width="10%">Aula</th>
            <th>Título</th>
            <th width="30%">Texto</th>
            <th width="20%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($aulas as $v)
        <tr>
            <td>
                <?php
                $video = $v->video;
                if(strpos($video,'youtube') !== false || strpos($video,'youtu.be') !== false){
                    $video = Youtube::embed($video);
                }else if(strpos($video,'vimeo') !== false){
                    $video = Vimeo::embed($video);
                }
                ?>
                <a href='{!! $video !!}' rel="fotos_{{$v->capitulo_id}}" class="title fancybox.iframe" title="Assistir {{$v->nome}}" data-toggle="tooltip" data-placement="right">
                    <img src="{{asset('images/play_video.png')}}" alt="Assistir Vídeo" style="width: 32px;">
                </a>
            </td>
            <td>{{$v->nome}}</td>
            <td>{{substr(strip_tags($v->texto),0,30)}}...</td>
            <td>
                <a href="{{ route('admin.aulas.editar',['id' => $v->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> editar</a>
                <a href="{{ route('admin.aulas.delete',['id' => $v->id]) }}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash" aria-hidden="true"></i> excluir</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $aulas->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.fancybox')
    @include('includes.delete_alert')
    @include('includes.bootstrap_tooltip')
@endsection