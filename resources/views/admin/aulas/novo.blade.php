@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Texto</h3>

        <br>
        <a href="{{ route('admin.aulas.index',['capitulo_id' => $capitulo_id]) }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => ['admin.aulas.salvar', 'capitulo_id' => $capitulo_id], 'class' => 'form']) !!}

        @include('admin.aulas.form')

        {!! Form::close() !!}

    </div>

@endsection