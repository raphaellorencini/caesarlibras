@extends('admin')

@section('content')

    <div class="container">
        <h3><strong>{{$capitulo->nome}}</strong> - Textos</h3>

        <br>
        <a href="{{ route('admin.aulas.index',['capitulo_id' => $capitulo_id]) }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($aula, ['route' => ['admin.aulas.atualizar', $aula->id], 'class' => 'form']) !!}

        @include('admin.aulas.form')

        {!! Form::close() !!}

    </div>

@endsection