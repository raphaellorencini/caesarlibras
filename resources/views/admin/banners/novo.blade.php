@extends('admin')

@section('content')

    <div class="container">
        <h3>Novo Banner</h3>

        <br>
        <a href="{{ route('admin.banners.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::open(['route' => 'admin.banners.salvar', 'class' => 'form']) !!}

        @include('admin.banners.form')

        {!! Form::close() !!}

    </div>

@endsection