<?php
if($teste !== false){
    $pagseguro_url = 'https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js';
    $pagseguro_lightbox_url = 'https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html';
}else{
    $pagseguro_url = 'https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js';
    $pagseguro_lightbox_url = 'https://pagseguro.uol.com.br/checkout/v2/payment.html';
}
?>
<script type="text/javascript" src="{{$pagseguro_url}}"></script>
<script type="text/javascript">
    function pagseguro() {
        $(function(){
            $('#ajax_load').show();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.post(
                '{{route('site.pagseguro')}}',
                function(data){
                    if(data !== '' && data !== undefined) {
                        location.href="{{$pagseguro_lightbox_url}}?code="+data;
                    }
                }
            );
        });
    }
</script>