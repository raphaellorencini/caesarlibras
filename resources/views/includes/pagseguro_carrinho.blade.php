<script src="{{asset('js/autonumeric/autoNumeric-min.js')}}"></script>
<script>
    function adicionados(){
        @foreach(session('carrinho') as $v)
        $('#adicionar-{{$v['id']}}').addClass('hidden');$('#adicionado-{{$v['id']}}').removeClass('hidden');
        @endforeach
    }
    function total_atualiza(){
        var total = 0;
        var valor = 0;
        $('input.precos').each(function(){
            valor = parseFloat($(this).val());
            total += valor;
        });
        $('#total').autoNumeric('destroy').autoNumeric('init',{
            aSep: '.',
            aDec: ',',
            dGroup: '2'
        });
        $('#total').autoNumeric('set', total);
    }
    $(function(){
        adicionados();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('a.carrinho-adicionar').click(function(){
            var valor = $(this).attr('data-cod');
            if(valor !== ''){
                $('#load-'+valor).removeClass('hidden');
                $.post(
                    '{{route('site.pagseguro_adicionar')}}',
                    {cod: valor},
                    function(d){
                        $('#load-'+valor).addClass('hidden');
                        if(d.adicionado === '1'){
                            $('#adicionar-'+valor).addClass('hidden');
                            $('#adicionado-'+valor).removeClass('hidden');
                        }
                    },'json'
                );
            }
            return false;
        });

        $('a.carrinho-remover').click(function(){
            var valor = $(this).attr('data-cod');
            if(valor !== ''){
                $('#load-'+valor).removeClass('hidden');
                $.post(
                    '{{route('site.pagseguro_remover')}}',
                    {cod: valor},
                    function(d){
                        $('#load-'+valor).addClass('hidden');
                        if(d.removido === '1'){
                            $('#linha-'+valor).remove();
                            total_atualiza();
                        }
                    },'json'
                );
            }
            return false;
        });
    });
</script>
