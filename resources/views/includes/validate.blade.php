<script src="{{ URL::asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery-validation/localization/messages_pt_BR.min.js') }}"></script>
<script>
    $.validator.addClassRules({
        required: {
            required: true
        },
        minlength: {
            minlength: 3
        },
        currency: {
            required: true
        },
        email: {
            email: true
        },
        senha: {
            required: true,
            minlength: 6
        },
        repetir_senha: {
            equalTo: '.senha'
        }
    });
    // override jquery validate plugin defaults
    $.validator.setDefaults({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'validate-error',
        errorPlacement: function(error, element) {
            error.insertAfter(element);
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    $(function(){
        var form = "form";
        $(form).validate({
            //debug: true,
        });
    });
</script>