@extends('layout')

@section('content')
    <section class="white section">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>Galeria de Fotos</h4>
                            <hr>
                        </div>

                        @foreach($fotos as $v)
                            <div class="row">
                                <div class="col-lg-3">
                                    <div>
                                        <a href="{{route('site.fotos_visualizar',['id' => $v->id])}}" rel="fotos_{{$v->id}}" class="title" title="{{$v->nome}}">
                                            <img src="{{asset($v->arquivo)}}" alt="" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-9" style="padding-top: 0; margin-top: 0;">
                                    <h4 style="padding-top: 0; margin-top: 0;">{{$v->nome}}</h4>
                                    <div>{{$v->descricao}}</div>
                                </div>
                            </div>
                            <br><br>

                        @endforeach

                    </div>
                </div>
            </div>

        </div>
    </section><!-- end section -->

@endsection