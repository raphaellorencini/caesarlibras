@extends('layout')

@section('content')
    <section class="white section">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$noticia->titulo}}</h4>
                            <hr>
                        </div>

                        <time datetime="{{substr($noticia->created_at,0,10)}}">{{Formata::date(substr($noticia->created_at,0,10),'.')}}</time>
                        @if(!empty($noticia->arquivo))
                            <figure><img src="{{URL::asset($noticia->arquivo)}}" alt="" class="img-responsive"></figure>
                        @endif
                        <br>
                        <div class="texto">{!! $noticia->texto !!}</div>
                        <br><br>

                    </div>
                </div>
            </div>

        </div>
    </section><!-- end section -->

@endsection