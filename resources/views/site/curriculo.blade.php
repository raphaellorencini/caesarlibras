@extends('layout')

@section('content')

    <section class="white section">
        <div class="container">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[0]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[0]['texto'] !!}
                    </div>
                </div>
            </div>

         </div>   
    </section><!-- end section -->

@endsection