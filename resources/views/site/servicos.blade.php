@extends('layout')

@section('content')

    <section class="white section">
        <div class="container">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <a id="servicos"></a>
                            <h4>{{$texto[0]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[0]['texto'] !!}

                        <div class="widget-title">
                            <h4>{{$texto[1]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[1]['texto'] !!}
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <a class="ancora" id="contratacao"></a>
                            <h4>{{$texto[2]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[2]['texto'] !!}
                    </div>
                </div>

            </div>
         </div>   
    </section><!-- end section -->

@endsection