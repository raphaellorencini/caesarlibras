@extends('layout')

@section('content')

    <section class="white section">
        <div class="container">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[0]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[0]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[1]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[1]['texto'] !!}
                        <div class="row">
                            <div class="col-md-4">
                                <h4>Parte 1</h4>
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/slN9I_5UqU8" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-4">
                                <h4>Parte 2</h4>
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/4WlTd2yRpa0" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-4">
                                <h4>Parte 3</h4>
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/f19qJ8Ly6Q0" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-4">
                                <h4>Parte 4</h4>
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/oT7dXw7z2uU" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-4">
                                <h4>Parte 5</h4>
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/EjKygngDLcI" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-4">
                                <h4>Parte 6</h4>
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/5A6dcUWyOok" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-4">
                                <h4>Parte 7</h4>
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/NS_-VYTsfqs" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[2]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[2]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[3]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[3]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[4]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[4]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[5]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[5]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[6]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[6]['texto'] !!}
                    </div>
                </div>
            </div>

         </div>   
    </section><!-- end section -->

@endsection