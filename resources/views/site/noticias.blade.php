@extends('layout')

@section('content')
    <section class="white section">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>Últimas Notícias</h4>
                            <hr>
                        </div>

                        @foreach($noticias as $v)
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="thumb-pad5 maxheight">
                                    <div class="thumbnail">
                                        <figure style="height: 191px; overflow: hidden;">
                                            @if(!empty($v->arquivo))
                                                <a href="{{route('site.noticia',['slug'=>$v->slug])}}"><img src="{{URL::asset($v->arquivo)}}" alt=""></a>
                                            @else
                                                <a href="{{route('site.noticia',['slug'=>$v->slug])}}"><img src="{{URL::asset('images/img-vazio.png')}}" alt=""></a>
                                            @endif
                                        </figure>
                                        <div class="caption">
                                            <time datetime="{{substr($v->created_at,0,10)}}">{{Formata::date(substr($v->created_at,0,10),'.')}}</time>
                                            <br>
                                            <a href="{{route('site.noticia',['slug'=>$v->slug])}}" class="description">{{$v->titulo}}</a>
                                            <p>{{Texto::resumo($v->texto)}}...</p>
                                            <a href="{{route('site.noticia',['slug'=>$v->slug])}}" class="btn-default btn1">mais...</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 centre-box">
                            <h2>Mais Artigos</h2>
                            @foreach($noticias2 as $v)
                                <div class="row">
                                    <div class="col-lg-3">
                                        <figure style="width:100%; overflow: hidden;">
                                            @if(!empty($v->arquivo))
                                                <a href="{{route('site.noticia',['slug'=>$v->slug])}}"><img src="{{URL::asset($v->arquivo)}}" alt="" style="width: 100%;"></a>
                                            @else
                                                <a href="{{route('site.noticia',['slug'=>$v->slug])}}"><img src="{{URL::asset('images/img-vazio.png')}}" alt=""></a>
                                            @endif
                                        </figure>
                                    </div>
                                    <div class="col-lg-9">
                                        <time datetime="{{substr($v->created_at,0,10)}}"><a href="{{route('site.noticia',['slug'=>$v->slug])}}">{{Formata::date(substr($v->created_at,0,10),'.')}}</a></time>
                                        <br>
                                        <a href="{{route('site.noticia',['slug'=>$v->slug])}}" class="description">{{$v->titulo}}</a>
                                        <p>{{Texto::resumo($v->texto)}}...</p>
                                    </div>
                                </div>
                                <div style="width: 100%; margin-top: 15px; border-top: 1px solid #F2F2F2;"></div>
                            @endforeach
                            <div>{!! $noticias2->render() !!}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- end section -->

@endsection