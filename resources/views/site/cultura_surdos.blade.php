@extends('layout')

@section('content')

    <section class="white section">
        <div class="container">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[0]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[0]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[1]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[1]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[2]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[2]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[3]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[3]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[4]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[4]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[5]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[5]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[6]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[6]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[7]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[7]['texto'] !!}
                    </div>
                </div>
            </div>
            <hr>

         </div>   
    </section><!-- end section -->

@endsection