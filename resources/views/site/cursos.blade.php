@extends('layout')

@section('content')
    <section class="white section">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>Cursos</h4>
                            <hr>
                        </div>

                        <div class="row">
                            @foreach($cursos_lista as $v)
                            <div class="col-lg-4">
                                <div class="">
                                    <div class="curso curso-titulo text-center">
                                        <a href="{{route('site.curso',['id' => $v->id])}}"><h1>{{$v->nome}}</h1></a>
                                    </div>
                                    <div class="curso curso-img">
                                        <a href="{{route('site.curso',['id' => $v->id])}}"><img src="{{asset($v->arquivo)}}" alt="" class="img-responsive"></a>
                                    </div>

                                    <div class="text-center">
                                        {!! $v->valor_original !!} {!!$v->valor_final!!} <br>
                                        em até <span class="curso-parcela">12x</span> {!!$v->valor_parcelado!!} pelo
                                        <img src="{{asset('images/pagseguro.png')}}" alt="" width="150">
                                    </div>
                                    <br>
                                    <div class="text-center">
                                        <a href="{{route('site.curso',['id' => $v->id])}}" class="btn-sm btn-info"><i class="fa fa-search-plus" aria-hidden="true"></i> Detalhes</a>
                                        <a id="adicionar-{{$v->id}}" href="javascript:;" class="btn-sm btn-danger carrinho-adicionar" data-cod="{{$v->id}}"><i class="fa fa-cart-plus" aria-hidden="true"></i> Adicionar ao Carrinho</a>
                                        <a id="adicionado-{{$v->id}}" href="javascript:;" class="btn-sm btn-success carrinho-adicionado hidden"><i class="fa fa-check-circle" aria-hidden="true"></i> Adicionado</a>
                                        <span id="load-{{$v->id}}" class="hidden"><img src="{{asset('images/loading.gif')}}" alt="" width="16"></span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div>{!! $cursos->render() !!}</div>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- end section -->

@endsection
@section('scripts')
    @include('includes.pagseguro_carrinho')
@endsection