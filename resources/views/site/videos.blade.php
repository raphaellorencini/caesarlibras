@extends('layout')

@section('content')
    <section class="white section">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        @foreach($fotos as $v)
                            <div class="widget-title">
                                <h4>{{$v->nome}}</h4>
                                <hr>
                            </div>

                            <div class="row">
                                <?php $i = 0;?>
                                @foreach($v->fotos as $v2)
                                    <div class="col-lg-3">
                                        <a href="{{Youtube::embed($v2->arquivo)}}" rel="fotos_{{$v2->id}}" class="title fancybox.iframe" title="{{$v2->legenda}}">
                                            {!! Youtube::thumb($v2->arquivo) !!}
                                        </a>
                                    </div>
                                    <?php $i++; ?>
                                    @if($i % 4 == 0 && $i > 0)
                                        <br style="clear: both"><br>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end section -->

@endsection
@section('scripts')
    @include('includes.fancybox')
@endsection