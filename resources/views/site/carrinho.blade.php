@extends('layout')

@section('content')
    <section class="white section">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>Carrinho de Compras</h4>
                            <hr>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="170"></th>
                                <th>Curso</th>
                                <th width="15%">Preço</th>
                                <th width="15%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total = 0;
                            ?>
                            @foreach(session('carrinho') as $v)
                                <?php
                                $v = (Object)$v;
                                $total += $v->preco2
                                ?>
                            <tr id="linha-{{$v->id}}">
                                <td><img src="{{asset($v->imagem)}}" alt="" width="150"></td>
                                <td>{{$v->nome}}</td>
                                <td>R$ {{$v->preco}}<input type="hidden" value="{{$v->preco2}}" class="precos"></td>
                                <td>
                                    <a id="remover-{{$v->id}}" href="javascript:;" class="btn-sm btn-danger carrinho-remover" data-cod="{{$v->id}}"><i class="fa fa-trash" aria-hidden="true"></i> Remover</a>
                                    <span id="load-{{$v->id}}" class="hidden"><img src="{{asset('images/loading.gif')}}" alt="" width="16"></span>
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td class="text-right"><strong>Total:</strong></td>
                                <td>R$ <span id="total">{{Formata::currency($total)}}</span></td>
                                <td><a href="" id="comprar" class="btn-sm btn-success"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Finalizar Pedido</a></td>
                            </tr>
                            </tbody>
                        </table>


                        <form id="carrinho_form" action="{{$carrinho_url}}" method="post">

                            <input type="hidden" name="code" id="code" value="" />

                        </form>

                    </div>
                </div>
            </div>

        </div>
    </section><!-- end section -->

@endsection
@section('scripts')
    @include('includes.pagseguro_carrinho')
    @include('includes.pagseguro',['teste' => true])
    <script>
        $('#comprar').click(function(){
            pagseguro();
            return false;
        });
    </script>
@endsection