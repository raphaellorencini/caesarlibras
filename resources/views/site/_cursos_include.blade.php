<div class="row">
    <div class="col-md-12">
        <div class="section-title text-center">
            <h4>Cursos</h4>
            <p>Cursos de LIBRAS - Nível I, II, III</p>
        </div>
    </div><!-- end col -->
</div><!-- end row -->

<div id="owl-featured" class="owl-custom">
    <div class="owl-featured">
        <div class="shop-item-list entry">
            <div class="">
                <img src="images/site/course_01.png" alt="">
                <div class="magnifier">
                </div>
            </div>
            <div class="shop-item-title clearfix">
                <h4><a href="#">Curso Nível I</a></h4>
                <div class="shopmeta">
                    <span class="pull-left">12 Alunos</span>
                    <div class="rating pull-right">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div><!-- end rating -->
                </div><!-- end shop-meta -->
            </div><!-- end shop-item-title -->
            <div class="visible-buttons">
                <a title="Adicionar ao carrinho" href="#"><span class="fa fa-cart-arrow-down"></span></a>
                <a title="Leia mais" href="#"><span class="fa fa-search"></span></a>
            </div><!-- end buttons -->
        </div><!-- end relative -->
    </div><!-- end col -->

    <div class="owl-featured">
        <div class="shop-item-list entry">
            <div class="">
                <img src="images/site/course_02.png" alt="">
                <div class="magnifier">
                </div>
            </div>
            <div class="shop-item-title clearfix">
                <h4><a href="#">Curso Nível II</a></h4>
                <div class="shopmeta">
                    <span class="pull-left">21 Alunos</span>
                    <div class="rating pull-right">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                    </div><!-- end rating -->
                </div><!-- end shop-meta -->
            </div><!-- end shop-item-title -->
            <div class="visible-buttons">
                <a title="Adicionar ao carrinho" href="#"><span class="fa fa-cart-arrow-down"></span></a>
                <a title="Leia mais" href="#"><span class="fa fa-search"></span></a>
            </div><!-- end buttons -->
        </div><!-- end relative -->
    </div><!-- end col -->

    <div class="owl-featured">
        <div class="shop-item-list entry">
            <div class="">
                <img src="images/site/course_03.png" alt="">
                <div class="magnifier">
                </div>
            </div>
            <div class="shop-item-title clearfix">
                <h4><a href="#">Curso Nível III</a></h4>
                <div class="shopmeta">
                    <span class="pull-left">98 Alunos</span>
                    <div class="rating pull-right">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                    </div><!-- end rating -->
                </div><!-- end shop-meta -->
            </div><!-- end shop-item-title -->
            <div class="visible-buttons">
                <a title="Adicionar ao carrinho" href="#"><span class="fa fa-cart-arrow-down"></span></a>
                <a title="Leia mais" href="#"><span class="fa fa-search"></span></a>
            </div><!-- end buttons -->
        </div><!-- end relative -->
    </div><!-- end col -->

    <div class="owl-featured">
        <div class="shop-item-list entry">
            <div class="">
                <img src="images/site/course_04.png" alt="">
                <div class="magnifier">
                </div>
            </div>
            <div class="shop-item-title clearfix">
                <h4><a href="#">Pacote de Cursos</a></h4>
                <div class="shopmeta">
                    <span class="pull-left">98 Alunos</span>
                    <div class="rating pull-right">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                    </div><!-- end rating -->
                </div><!-- end shop-meta -->
            </div><!-- end shop-item-title -->
            <div class="visible-buttons">
                <a title="Adicionar ao carrinho" href="#"><span class="fa fa-cart-arrow-down"></span></a>
                <a title="Leia mais" href="#"><span class="fa fa-search"></span></a>
            </div><!-- end buttons -->
        </div><!-- end relative -->
    </div><!-- end col -->
</div><!-- end owl-featured -->