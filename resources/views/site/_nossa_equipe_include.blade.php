<div class="row">
    <div class="col-md-12">
        <div class="section-title text-center">
            <h4>Nossa Equipe</h4>
            <p>Conheça nossos prestadores de serviço eventuais</p>
        </div>
    </div><!-- end col -->
</div><!-- end row -->
<div class="row">
    @foreach($equipe as $v)
    <div class="col-lg-3 col-md-3 col-sm-12" style="height: 220px;">
        <div class="testimonial">
            <img class="alignleft img-circle" src="{{URL::asset($v->imagem)}}" alt="">
            {!! $v->texto !!}
            <div class="testimonial-meta">
                <h4>{{$v->nome}}</h4>
            </div>
        </div><!-- end dmbox -->
    </div>
    @endforeach
</div><!-- end row -->
<div class="button-wrapper text-center">
    <p>
        Somos uma Empresa especializada na Língua Brasileira de Sinais.
        Trabalhamos na prestação de serviços de compreensão e aplicação da Língua Brasileira de Sinais em treinamentos, eventos, seminários, congressos e palestras.
    </p>
</div><!-- end button-wrapper -->