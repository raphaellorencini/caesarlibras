@extends('layout')

@section('content')
    <section class="white section">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">

                        <div class="content-widget">
                            <div class="widget-title">
                                <h4>{{$curso->nome}}</h4>
                                <hr>
                            </div>
                            @if(!empty($curso->arquivo))
                                <figure><img src="{{URL::asset($curso->arquivo)}}" alt="" class="img-responsive"></figure>
                            @endif
                            <br>

                            <div>
                                {!! $valor_original !!} {!!$valor_final!!} <br>
                                em até <span class="curso-parcela">12x</span> {!!$valor_parcelado!!} pelo
                                <img src="{{asset('images/pagseguro.png')}}" alt="" width="150">
                            </div>
                            <div class="text-left">
                                <a id="adicionar-{{$curso->id}}" href="javascript:;" class="btn-sm btn-danger carrinho-adicionar" data-cod="{{$curso->id}}"><i class="fa fa-cart-plus" aria-hidden="true"></i> Adicionar ao Carrinho</a>
                                <a id="adicionado-{{$curso->id}}" href="javascript:;" class="btn-sm btn-success carrinho-adicionado hidden"><i class="fa fa-check-circle" aria-hidden="true"></i> Adicionado</a>
                                <span id="load-{{$curso->id}}" class="hidden"><img src="{{asset('images/loading.gif')}}" alt="" width="16"></span>
                            </div>
                            <br>
                            <div class="texto">{!! $curso->texto !!}</div>
                            <br><br>

                        </div>
                        
                        
                    </div>
                </div>
            </div>

        </div>
    </section><!-- end section -->

@endsection
@section('scripts')
    @include('includes.pagseguro_carrinho')
@endsection