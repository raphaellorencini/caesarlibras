@extends('layout')

@section('content')

    <section class="white section">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[0]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[0]['texto'] !!}
                    </div><!-- end content-widget -->
                </div><!-- end col -->

                <div class="col-md-6">
                    <div class="media-element">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="images/slide2_01.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="images/slide2_02.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="images/slide2_03.jpg" alt="" class="img-responsive">
                                </div>
                            </div>

                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div><!-- end blog-image -->
                </div><!-- end col -->
            </div><!-- end row -->

            <hr class="invis1">

            <div class="row">
                <div class="col-md-12">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>{{$texto[1]['nome']}}</h4>
                            <hr>
                        </div>
                        {!! $texto[1]['texto'] !!}
                    </div><!-- end content-widget -->
                </div><!-- end col -->
            </div>
        </div><!-- end container -->
    </section><!-- end section -->

    <hr class="invis1">
    <section class="grey section">
        <div class="container">
            @include('site._nossa_equipe_include')
        </div><!-- end container -->
    </section><!-- end section -->

    <section class="white section">
        <div class="container">


            <div class="content-widget">
                <div class="widget-title">
                    <h4>Nossos Clientes</h4>
                    <hr>
                </div>
            </div><!-- end row -->

            <div class="row">
                <?php
                $i = 0;
                ?>
                @foreach($clientes as $v)
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div style="height: 116px; margin-bottom: 15px;">
                            <img src="{{URL::asset($v->imagem)}}" alt="" class="img-responsive img-thumbnail">
                        </div>
                    </div>
                    @if($i > 0 && $i % 5 == 0)
                        <br style="clear: both">
                    @endif
                    <?php
                        $i++;
                    ?>
                @endforeach
            </div><!-- end button-wrapper -->
        </div><!-- end container -->
    </section><!-- end section -->

@endsection