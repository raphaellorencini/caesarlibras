@extends('layout')

@section('content')
<section class="slider-section">
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                @foreach($banners as $v)
                <li data-transition="fade" data-slotamount="1" data-masterspeed="500" data-thumb="{{$v->arquivo}}"  data-saveperformance="off"  data-title="">
                    <img src="{{$v->arquivo}}"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <div class="tp-caption {{$v->class}} text-center lft tp-resizeme"
                         data-x="center"
                         data-y="{{$v->posicao_legenda}}"
                         data-speed="1000"
                         data-start="600"
                         data-easing="Power3.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">{!! $v->legenda !!}
                    </div>
                    <div class="tp-caption {{$v->class2}} text-center lft tp-resizeme"
                         data-x="center"
                         data-y="{{$v->posicao_legenda2}}"
                         data-speed="1000"
                         data-start="800"
                         data-easing="Power3.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">{!! $v->legenda2 !!}
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</section>

<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div>
                    {!! $texto[0]['texto'] !!}
                </div>
            </div>
        </div>
    </div><!-- end container -->
</section><!-- end section -->

<section class="grey section">
    <div class="container">
        @include('site._nossa_equipe_include')
    </div><!-- end container -->
</section><!-- end section -->

<article id="contact" class="map-section">
    <div id="map" class="wow slideInUp"></div>
</article><!-- end section -->
@endsection

@section('scripts')
    <!-- CUSTOM PLUGINS -->
    <script src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDiRQbXAsi6Ta5nh-ZlTkispuLOQhls_SA"></script>
    <script src="{{URL::asset('js/contact.js')}}"></script>
    <script src="{{URL::asset('js/map.js')}}"></script>
@endsection