@extends('layout')

@section('content')
    <section class="white section">
        <div class="container">
            <div class="row contact-wrapper">
                <div class="col-md-9 col-sm-9 col-xs-12 content-widget">
                    <div class="widget-title">
                        <h4>Formulário de Contato</h4>
                        <hr>
                    </div>
                    <div id="contato" class="contact_form row">
                        <div id="message"></div>
                        <form id="contato_form" action="{{route('site.contato_enviar')}}" name="contato_form" method="post">
                            {!! Form::token() !!}

                            @if(!empty(Session::get('enviado')))
                                <div class="alert alert-success" role="alert">
                                    {{Session::get('enviado')}}
                                </div>
                                <br style="clear: both">
                            @endif

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="label-site">
                                    <input type="text" name="nome" id="nome" class="form-control required" placeholder="Nome *">
                                    <span class="error-message"></span>
                                </label>
                                <label class="label-site">
                                    <input type="text" name="email" id="email" class="form-control required email" placeholder="E-mail *">
                                    <span class="error-message"></span>
                                </label>
                                <label class="label-site">
                                    <input type="text" name="telefone" id="telefone" class="form-control" placeholder="Telefone">
                                    <span class="error-message"></span>
                                </label>

                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <textarea class="form-control" name="mensagem" id="mensagem" rows="6" placeholder=""></textarea>
                                <button type="submit" value="enviar" id="submit" class="btn btn-primary btn-block">Enviar</button>
                            </div>
                        </form>
                    </div><!-- end contact-form -->
                </div><!-- end col -->

                <div class="col-md-3 col-sm-3 col-xs-12 content-widget">
                    <div class="widget-title">
                        <h4>Fale Conosco</h4>
                        <hr>
                    </div>
                    <div class="contact-list contato-texto">
                        @include('site._contato_detalhes_include')
                    </div><!-- end contact-list -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery-validation/localization/messages_pt_BR.min.js') }}"></script>
    <script>
        $(function(){
            $.validator.addClassRules({
                required: {
                    required: true
                },
                minlength: {
                    minlength: 3
                },
                currency: {
                    required: true
                },
                email:{
                    email: true
                }
            });
            $('#contato_form').validate({
                //debug: true,
                errorClass:'error-message',
                validClass:'success',
                errorElement:'label',
                highlight: function (element, errorClass, validClass) {
                    $(element).parents("div.form-div-4").addClass("error-message has-error");

                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".error-message").removeClass("error-message has-error");
                }
            });
        });
    </script>
@endsection