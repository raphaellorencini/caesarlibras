<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex,nofollow">
    <title>Caesarlibras Painel</title>

    <!--CSS-->
    <link rel="stylesheet" href="{{URL::asset('css/admin_bootstrap.css')}}" >
    <link rel="stylesheet" href="{{URL::asset('fonts/font-awesome-4.6.3/css/font-awesome.min.css')}}">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{{URL::asset('css/ie10-viewport-bug-workaround.css')}}" rel="stylesheet">

    <script src="{{URL::asset('js/ie-emulation-modes-warning.js')}}"></script>
    <!--[if lt IE 9]><script src="{{URL::asset('js/ie8-responsive-file-warning.js')}}"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{URL::asset('js/html5shiv.js')}}"></script>
    <script src="{{URL::asset('js/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="{{URL::asset('images/logo.png')}}" alt="" height="50">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                @if(!empty(Auth::user()) && Auth::user()->role == 'admin')
                    <li><a href="{{ route('admin.home') }}">Home</a></li>
                    <li><a href="{{ route('admin.paginas.index') }}">Páginas e Textos</a></li>
                    <li><a href="{{ route('admin.banners.index') }}">Banners</a></li>
                    <li><a href="{{ route('admin.fotos_categorias.index') }}">Fotos</a></li>
                    <li><a href="{{ route('admin.fotos.index',['foto_categoria_id' => 1]) }}">Vídeos</a></li>
                    <li><a href="{{ route('admin.fotos.index',['foto_categoria_id' => 7]) }}">Clientes</a></li>
                    <li><a href="{{ route('admin.noticias.index') }}">Notícias</a></li>
                    <li><a href="{{ route('admin.colaboradores.index') }}">Colaboradores</a></li>
                    {{--<li><a href="{{ route('admin.cursos.index') }}">Cursos</a></li>--}}
                    <li><a href="{{ route('admin.users.index') }}">Usuários</a></li>
                @endif
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if(auth()->guest())
                    @if(!Request::is('auth/login'))
                        <li><a href="{{ url('/auth/login') }}">Entrar</a></li>
                    @endif
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">{{ auth()->user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('admin.users.editar',['id' => Auth::user()->id]) }}">
                                    <i class="fa fa-user" aria-hidden="true"></i> Perfil
                                </a>
                            </li>
                            <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Sair</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

@yield('content')

<!--JS-->
<script src="{{URL::asset('js/jquery.min.js')}}"></script>
<script src="{{URL::asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{URL::asset('js/admin_bootstrap.min.js')}}"></script>
@yield('scripts')
</body>
</html>
