<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <title>Caesarlibras</title>

    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::asset('images/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{URL::asset('images/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::asset('images/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('images/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::asset('images/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL::asset('images/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL::asset('images/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL::asset('images/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{URL::asset('images/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{URL::asset('images/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL::asset('images/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{URL::asset('images/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL::asset('images/favicon-16x16.png')}}">
    <link rel="manifest" href="{{URL::asset('images/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{URL::asset('images/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <meta name="robots" content="index,follow">

    <link rel="stylesheet" type="text/css" href="{{URL::asset('rs-plugin/css/settings.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('fonts/font-awesome-4.6.3/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/bxslider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('style.css')}}">

    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/custom.css')}}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="'https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
</head>
<body>

<div id="wrapper">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <p>
                        <img src="{{URL::asset('images/maos_32x32.png')}}" alt="">
                        Profissionais capacitados e certificados.
                    </p>
                </div><!-- end left -->

                <div class="col-md-6 text-right">
                    <ul class="list-inline">
                        <li>
                            <a class="social" href="https://www.facebook.com/CaesarlibrasCursosTecnicos/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a class="social" href="https://twitter.com/caesarlibras" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a class="social" href="https://www.youtube.com/channel/UCCCn0d8ApESFq4GqKVV4Syw" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                            {{--<a class="social" href="#"><i class="fa fa-google-plus"></i></a>
                            <a class="social" href="#"><i class="fa fa-linkedin"></i></a>--}}
                        </li>
                        {{--<li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-lock"></i> Login & Cadastro</a>--}}
                            {{--<div class="dropdown-menu">
                                <form method="post">
                                    <div class="form-title">
                                        <h4>Login</h4>
                                        <hr>
                                    </div>
                                    <input class="form-control" type="text" name="username" placeholder="Usuário">
                                    <div class="formpassword">
                                        <input class="form-control" type="password" name="password" placeholder="******">
                                    </div>
                                    <div class="clearfix"></div>
                                    <button type="submit" class="btn btn-block btn-primary">Entrar</button>
                                    <hr>
                                    <h4><a href="#">Cadastrar nova conta</a></h4>
                                </form>
                            </div>--}}
                        {{--</li>--}}
                    </ul>
                </div><!-- end right -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end topbar -->

    @include('menu')

    @yield('content')

    <footer class="dark footer section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="widget">
                        <div class="widget-title">
                            <h4>Sobre Caesarlibras</h4>
                            <hr>
                        </div>
                        <p>Somos uma Empresa pioneira voltada para a acessibilidade da pessoa com deficiencia promovendo através de cursos e palestras a eliminação das barreiras comunicacionais e atitudinais. </p>

                        <a href="{{route('site.quem_somos')}}" class="btn btn-default">Leia mais</a>
                    </div><!-- end widget -->
                </div><!-- end col -->

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="widget">
                        <div class="widget-title">
                            <h4>Tweets Recentes</h4>
                            <hr>
                        </div>

                        <div class="twitter-widget">
                            {{--<ul class="latest-tweets">
                                <li>
                                    <p><a href="#" title="">@Caesarlibras</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                    <span>2 horas atrás</span>
                                </li>
                                <li>
                                    <p><a href="#" title="">@Caesarlibras</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                    <span>2 horas atrás</span>
                                </li>
                            </ul>--}}<!-- end latest-tweet -->

                                <a class="twitter-timeline"
                                   data-lang="pt"
                                   data-width="360"
                                   data-height="271"
                                   data-theme="dark"
                                   data-link-color="#009F3C"
                                   data-tweet-limit="2"
                                   data-chrome="noheader nofooter noborders transparent"
                                   href="https://twitter.com/Caesarlibras"></a>
                                <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div><!-- end twitter-widget -->

                    </div><!-- end widget -->
                </div><!-- end col -->

                {{--<div class="col-md-3 col-md-6 col-xs-12">
                    <div class="widget">
                        <div class="widget-title">
                            <h4>Cursos</h4>
                            <hr>
                        </div>

                        <ul class="popular-courses">
                            <li>
                                <a href="#" title=""><img class="img-thumbnail" src="{{URL::asset('images/site/service_01.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a href="#" title=""><img class="img-thumbnail" src="{{URL::asset('images/site/service_02.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a href="#" title=""><img class="img-thumbnail" src="{{URL::asset('images/site/service_03.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a href="#" title=""><img class="img-thumbnail" src="{{URL::asset('images/site/service_04.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a href="#" title=""><img class="img-thumbnail" src="{{URL::asset('images/site/service_05.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a href="#" title=""><img class="img-thumbnail" src="{{URL::asset('images/site/service_06.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a href="#" title=""><img class="img-thumbnail" src="{{URL::asset('images/site/service_07.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a href="#" title=""><img class="img-thumbnail" src="{{URL::asset('images/site/service_08.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a href="#" title=""><img class="img-thumbnail" src="{{URL::asset('images/site/service_09.png')}}" alt=""></a>
                            </li>
                        </ul>
                    </div><!-- end widget -->
                </div><!-- end col -->--}}

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="widget">
                        <div class="widget-title">
                            <h4>Entre em contato conosco</h4>
                            <hr>
                        </div>

                        @include('site._contato_detalhes_include')

                    </div><!-- end widget -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </footer><!-- end section -->

    <section class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <p>© <?=date('Y')?> Caesarlibras</p>
                </div><!-- end col -->
                <div class="col-md-6 text-right">
                </div>
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->
</div><!-- end wrapper -->

<script src="{{URL::asset('js/jquery.min.js')}}"></script>
<script src="{{URL::asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/retina.js')}}"></script>
<script src="{{URL::asset('js/wow.js')}}"></script>
<script src="{{URL::asset('js/carousel.js')}}"></script>
<!-- CUSTOM PLUGINS -->
<script src="{{URL::asset('js/custom.js')}}"></script>
<!-- SLIDER REV -->
<script src="{{URL::asset('rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{URL::asset('rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{URL::asset('js/scripts.js')}}"></script>

@yield('scripts')
</body>
</html>