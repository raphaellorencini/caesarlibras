@extends('aluno')

@section('content')

<div class="container">
    <h3><i class="fa fa-graduation-cap" aria-hidden="true"></i> Cursos</h3>

    <br><br>

    <div class="row">
        @foreach($cursos as $v)
            <div class="col-lg-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i> {{$v->nome}}
                    </div>
                    <div class="panel-body text-center"><a href="{{route('aluno.cursos.aulas',['curso_id' => $v->id])}}"><img src="{{asset($v->arquivo)}}" alt="{{$v->nome}}"></a></div>
                </div>
            </div>
        @endforeach
    </div>

    {!! $cursos->render() !!}
</div>

@endsection
