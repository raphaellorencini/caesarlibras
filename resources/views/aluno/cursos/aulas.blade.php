@extends('aluno')

@section('content')

<div class="container">
    <h3><i class="fa fa-graduation-cap" aria-hidden="true"></i> {{$curso->nome}}</h3>

    <br>
    <a href="{{ route('aluno.cursos') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
    <br><br>

    <div class="panel-group" id="accordion">
    <?php
    $i = 1;
    ?>
    @foreach($capitulos as $v)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}"><i class="fa fa-bookmark" aria-hidden="true"></i> {{$v->nome}}</a>
                    </h3>
                </div>
                <div id="collapse{{$i}}" class="panel-collapse collapse">
                    <div class="panel-body">{!! $v->texto !!}</div>
                    <?php
                    $j = 1;
                    ?>
                    @foreach($v->aulas as $v2)
                        <?php
                        $video = $v2->video;
                        if(strpos($video,'youtube') !== false || strpos($video,'youtu.be') !== false){
                            $video = Youtube::embed($video);
                        }else if(strpos($video,'vimeo') !== false){
                            $video = Vimeo::embed($video);
                        }
                        $titulo = 'Vídeo '.$j.' - '.$v2->nome;
                        ?>
                        <div class="list-group aulas-videos">
                            <div data-id="{{$v2->id}}" class="list-group-item video">
                                <div>
                                <h5 class="list-group-item-heading">
                                    <a href='{!! $video !!}' id="video-link-{{$v2->id}}" rel="fotos__{{$v->id}}" class="title fancybox.iframe" title="Assistir: {{$titulo}}" data-toggle="tooltip" data-placement="right">
                                    <i class="fa fa-video-camera" aria-hidden="true"></i> {{$titulo}}
                                    </a>
                                </h5>
                                <a href='{!! $video !!}' id="video-link{{$v2->id}}" rel="fotos_{{$v->id}}" class="title fancybox.iframe list-group-item-text" title="Assistir: {{$titulo}}" data-toggle="tooltip" data-placement="right">
                                    <img src="{{asset('images/play_video.png')}}" alt="Assistir Vídeo" style="width: 24px;">
                                </a>
                                </div>
                                <br>
                                @if(!empty($v2->texto))
                                <p class="list-group-item-text">{!! $v2->texto !!}</p>
                                @endif
                            </div>
                        </div>
                        <?php
                        $j++;
                        ?>
                    @endforeach
                </div>
            </div>
            <?php
            $i++;
            ?>
    @endforeach
    </div>

</div>

@endsection

@section('scripts')
    @include('includes.fancybox')
    @include('includes.bootstrap_tooltip')
@endsection
