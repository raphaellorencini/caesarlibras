@extends('aluno')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-desktop" aria-hidden="true"></i> Área do Aluno</div>

                <div class="panel-body">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="botoes-home">
                                    <a href="{{ route('aluno.cursos') }}" class="btn btn-default btn-lg">
                                        <img src="{{asset('images/cursos.png')}}" alt="Cursos" style="width: 64px;"> Cursos
                                    </a>
                                    <a href="{{ route('aluno.pedidos.index') }}" class="btn btn-default btn-lg">
                                        <img src="{{asset('images/pedidos.png')}}" alt="Pedidos" style="width: 64px;"> Pedidos
                                    </a>
                                    <a href="{{ route('aluno.perfil.editar',['id' => Auth::user()->id]) }}" class="btn btn-default btn-lg">
                                        <img src="{{asset('images/perfil.png')}}" alt="Perfil" style="width: 64px;"> Perfil
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
