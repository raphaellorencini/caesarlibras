@if($tipo == 'novo' || $tipo == 'editar')
<div class="form-group">
    {!! Form::label('name','Nome:') !!}
    {!! Form::text('user[name]', null, ['class' => 'form-control required']) !!}
</div>

<div class="form-group">
    {!! Form::label('email','E-mail:') !!}
    {!! Form::text('user[email]', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone','Telefone:') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control tel']) !!}
</div>

<div class="form-group">
    {!! Form::label('celular','Celular:') !!}
    {!! Form::text('celular', null, ['class' => 'form-control cel']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco','Endereço:') !!}
    {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('bairro','Bairro:') !!}
    {!! Form::text('bairro', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cidade','Cidade:') !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('estado','Estado:') !!}
    {!! Form::select('estado', $estados, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cep','CEP:') !!}
    {!! Form::text('cep', null, ['class' => 'form-control cep']) !!}
</div>
    
@endif

@if($tipo == 'novo' || $tipo == 'editar_senha')
<div class="form-group">
    {!! Form::label('password','Senha:') !!}
    {!! Form::password('password', ['class' => 'form-control senha']) !!}
</div>

<div class="form-group">
    {!! Form::label('password2','Repetir Senha:') !!}
    {!! Form::password('password2', ['class' => 'form-control repetir_senha']) !!}
</div>
@endif

<div class="form-group">
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
</div>

@section('scripts')
    @include('includes.masks')
    @include('includes.validate')

    <script>
    @if($tipo == 'editar')
        $(function(){
            $('#email').attr('readonly','readonly');
        });
    @endif
    </script>
@endsection