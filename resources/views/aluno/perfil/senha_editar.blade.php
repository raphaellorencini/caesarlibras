@extends('aluno')

@section('content')

    <div class="container">
        <h3><i class="fa fa-user" aria-hidden="true"></i> Perfil - Mudar a Senha: {{ $aluno->user->name }}</h3>

        <br>
        <a href="{{ route('aluno.home') }}" class="btn btn-default"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
        <a href="{{ route('aluno.perfil.editar',['id' => $aluno->user->id]) }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar ao Perfil</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($aluno, ['route' => ['aluno.perfil.senha_atualizar', $aluno->id], 'class' => 'form']) !!}

        @include('aluno.perfil.form',['tipo' => 'editar_senha'])

        {!! Form::close() !!}

    </div>

@endsection