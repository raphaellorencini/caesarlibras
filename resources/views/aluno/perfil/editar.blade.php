@extends('aluno')

@section('content')

    <div class="container">
        <h3><i class="fa fa-user" aria-hidden="true"></i> Perfil: {{ $aluno->user->name }}</h3>

        <br>
        <a href="{{ route('aluno.home') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
        <a href="{{ route('aluno.perfil.senha_editar', ['id' => $aluno->user->id]) }}" class="btn btn-default"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Mudar a Senha</a>
        <br><br>

        @include('errors.mensagens')

        {!! Form::model($aluno, ['route' => ['aluno.perfil.atualizar', $aluno->id], 'class' => 'form']) !!}

        @include('aluno.perfil.form',['tipo' => 'editar'])

        {!! Form::close() !!}

    </div>

@endsection
