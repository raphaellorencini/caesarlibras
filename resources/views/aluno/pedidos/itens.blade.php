@extends('aluno')

@section('content')

<div class="container">
    <h3><i class="fa fa-shopping-cart" aria-hidden="true"></i> Pedido Número: {{$pedido->numero}}</h3>

    <br>
    <a href="{{ route('aluno.pedidos.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Pedidos</a>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Curso</th>
            <th width="15%">Preço</th>
        </tr>
        </thead>
        <tbody>
        @foreach($itens as $v)
        <tr>
            <td>{{$v->curso->nome}}</td>
            <td>R$ {{Formata::currency($v->preco)}}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>

@endsection
