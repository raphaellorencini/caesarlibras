@extends('aluno')

@section('content')

<div class="container">
    <h3><i class="fa fa-shopping-cart" aria-hidden="true"></i> Pedidos</h3>

    <br>
    <div class="row">
        <div class="col-lg-10">
            <form class="form-inline" action="{{ route('aluno.pedidos.index') }}" method="get">
                <div class="form-group" style="padding-right: 20px;">
                    <a href="{{ route('aluno.home') }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Voltar</a>
                </div>
                <div class="form-group">
                    <label for="">Filtro de busca</label>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="numero" id="numero" placeholder="número do pedido" value="{{!empty($params['numero']) ? $params['numero'] : null}}">
                </div>
                <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                <a href="{{ route('aluno.pedidos.index') }}" class="btn btn-default"><i class="fa fa-times-circle" aria-hidden="true"></i> Limpar</a>
            </form>
        </div>
    </div>
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Número</th>
            <th width="15%">Status</th>
            <th width="15%">Total</th>
            <th>Data</th>
            <th width="20%">Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pedidos as $v)
        <tr>
            <td>{{$v->numero}}</td>
            <td>{!! Status::pedido_status($v->status) !!}</td>
            <td>R$ {{Formata::currency($v->total)}}</td>
            <td>{{Formata::date($v->created_at)}}</td>
            <td>
                <a href="{{ route('aluno.pedidos.itens',['numero' => $v->numero]) }}" class="btn btn-info btn-xs"><i class="fa fa-file-text-o" aria-hidden="true"></i> itens</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $pedidos->render() !!}
</div>

@endsection

@section('scripts')
    @include('includes.delete_alert')
@endsection