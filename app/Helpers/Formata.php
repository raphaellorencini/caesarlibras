<?php

namespace App\Helpers;


class Formata
{
    /**
     * Converte número em formato moeda R$ 1.000,00
     *
     * @param $number
     * @return string
     */
    public static function currency($number)
    {
        return number_format($number, 2, ',', '.');
    }

    /**
     * Converte moeda em formato número 1000.00
     *
     * @param $number
     * @return float
     */
    public static function currency_db($number)
    {
        $n = str_replace('.','', $number);
        $n = str_replace(',', '.', $n);
        return (float)$n;
    }

    /**
     * Formata data de yyyy-mm-dd para dd/mm/yyy ou de yyyy-mm-dd hh:mm:ss para dd/mm/yyy hh:mm:ss
     *
     * @param $date
     *
     * @return null|string
     */
    public static function date($date, $separador = '/')
    {
        $d = explode('-', $date);
        $d2 = null;
        if(strpos($date,':') === false){
            $d2 = $d[2].$separador.$d[1].$separador.$d[0];
        }else{
            $d[2] = explode(' ',$d[2]);
            $h = $d[2][1];
            $d2 = $d[2][0].$separador.$d[1].$separador.$d[0].' '.$h;
        }

        return $d2;
    }

    /**
     * Converte data em formato db: dd/mm/yyyy -> yyyy-mm-dd
     *
     * @param $date
     * @return null|string
     */
    public static function date_db($date)
    {
        $d = explode('/', $date);
        $d2 = null;
        if(strpos($date,':') === false){
            $d2 = sprintf("%s-%s-%s",$d[2],$d[1],$d[0]);
        }else{
            $d[2] = explode(' ',$d[2]);
            $h = $d[2][1];
            $d2 = sprintf("%s-%s-%s %s",$d[2][0], $d[1], $d[0], $h);
        }

        return $d2;
    }
}