<?php

namespace App\Helpers;


class Vimeo
{
    /**
     * @param $url
     * @param bool $iframe - usar vimeo com iframe ou só a url
     * @param int $width
     * @param int $height
     * @return mixed
     */
    public static function embed($url, $iframe = false, $width = 420, $height = 315)
    {
        $p = [
            "\s*[a-zA-Z\/\/:\.]*vimeo.com\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)"
        ];
        $p = implode('|',$p);
        $pattern = "/{$p}/i";
        preg_match($pattern, $url, $matches);

        $replace = ['https://player.vimeo.com/video/','https://vimeo.com/'];
        $vimeo_id = str_replace($replace, '', $matches[0]);

        if($iframe) {
            $url2 = "<iframe src=\"https://player.vimeo.com/video/{$vimeo_id}\" width=\"{$width}\" height=\"{$height}\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
        }else{
            $url2 = "https://player.vimeo.com/video/{$vimeo_id}";
        }

        return $url2;
    }

    /**
     * @param $url
     * @param string $quality - default, mq, hq
     * @return mixed
     */
    public static function thumb($url, $quality = "default", $width = 200)
    {
        switch ($quality){
            case 'lq':
                $q = 'thumbnail_small';
                break;
            case 'hq':
                $q = 'thumbnail_large';
                break;
            default:
                $q = 'thumbnail_medium';
                break;
        }
        $p = [
            "\s*[a-zA-Z\/\/:\.]*vimeo.com\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)"
        ];
        $p = implode('|',$p);
        $pattern = "/{$p}/i";
        preg_match($pattern, $url, $matches);

        $replace = ['https://player.vimeo.com/video/','https://vimeo.com/'];
        $vimeo_id = str_replace($replace, '', $matches[0]);

        $data = json_decode(file_get_contents("http://vimeo.com/api/v2/video/$vimeo_id.json"),true);

        $img = "<img src='".$data[0][$q]."' style='width: ".$width."px;' \/>";

        return $img;
    }
}