<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoItem extends Model
{
    protected $table = 'pedidos_itens';
    protected $fillable = [
        'curso_id',
        'pedido_id',
        'preco',
        'qtd',
    ];

    public function curso()
    {
        return $this->hasOne(Curso::class, 'id', 'curso_id');
    }
}
