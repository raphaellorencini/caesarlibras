<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class NoticiaRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Noticia";
    }

    function ultimos($quantidade = 3)
    {
        return $this->model->where(['publicado' => 1])->orderBy('created_at','DESC')->limit($quantidade)->get();
    }

    function outras($ids_excluidos)
    {
        return $this->model->whereNotIn('id',$ids_excluidos)->orderBy('created_at','DESC')->paginate();
    }

    function exibir_por_slug($slug)
    {
        return $this->model->where(['slug' => $slug])->first();
    }
}