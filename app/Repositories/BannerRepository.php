<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class BannerRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Banner";
    }

    function listar()
    {
        $model = $this->model->all();
        return $model;
    }
}