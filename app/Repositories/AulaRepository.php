<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class AulaRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Aula";
    }

    function listar($params = null, $order_by = null, $limit = null)
    {
        $model = $this->filtros($params);

        if(empty($limit)) {
            if(empty($order_by)){
                $order_by = ['id','ASC'];
            }
            $model = $model->orderBy($order_by)->get();
        }else{
            if(empty($order_by)){
                $order_by = 'id';
            }
            $model = $model->orderBy($order_by)->limit($limit)->get();
        }
        return $model;
    }

    function listar_paginado($params = null, $order_by = null, $limit = null)
    {
        if(empty($order_by)){
            $order_by = ['id','ASC'];
        }
        $model = $this->filtros($params);
        $model = $model->orderBy($order_by[0], $order_by[1])->paginate($limit);

        return $model;
    }

    private function filtros($params)
    {
        $model = $this->model;
        if(!empty($params['busca'])){
            $model = $model->where('nome','like','%'.$params['busca'].'%');
        }
        if(!empty($params['capitulo_id'])){
            $model = $model->where(['capitulo_id' => $params['capitulo_id']]);
        }

        return $model;
    }
}