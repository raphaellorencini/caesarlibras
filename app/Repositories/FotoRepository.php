<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class FotoRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Foto";
    }

    function listar($foto_categoria_id = null, $order_by = null)
    {
        if(empty($order_by)){
            $order_by = ['created_at', 'DESC'];
        }
        if(empty($foto_categoria_id)){
            $model = $this->model->where(['foto_categoria_id' => $foto_categoria_id])->orderBy($order_by[0], $order_by[1])->paginate();
        }else{
            $model = $this->model->orderBy($order_by[0], $order_by[1])->paginate();
        }

        return $model;
    }

    function listar_foto_categoria($foto_categoria_id, $order_by = null)
    {
        if(empty($order_by)){
            $order_by = ['created_at', 'DESC'];
        }
        $model = $this->model->where(['foto_categoria_id' => $foto_categoria_id])->orderBy($order_by[0], $order_by[1])->paginate();

        return $model;
    }

    function ultimos($foto_categoria_id, $limit = 3)
    {
        $model = $this->model->where(['foto_categoria_id' => $foto_categoria_id])->orderBy('created_at', 'DESC')->limit($limit)->get();

        return $model;
    }
}