<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class VideoRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Video";
    }

    function ultimos($quantidade = 3)
    {
        return $this->model->limit($quantidade)->orderBy('id', 'DESC')->get();
    }
}