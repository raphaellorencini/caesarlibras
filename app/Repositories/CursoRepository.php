<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class CursoRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Curso";
    }

    function listar($params = null, $order_by = null, $limit = null)
    {
        $model = $this->filtros($params);

        if(empty($limit)) {
            if(empty($order_by)){
                $order_by = ['id','DESC'];
            }
            $model = $model->where(['publicado' => 1])->orderBy($order_by)->get();
        }else{
            if(empty($order_by)){
                $order_by = 'id';
            }
            $model = $model->where(['publicado' => 1])->orderBy($order_by)->limit($limit)->get();
        }
        return $model;
    }

    function listar_paginado($params = null, $order_by = null, $limit = null)
    {
        if(empty($order_by)){
            $order_by = ['id','DESC'];
        }
        $model = $this->filtros($params);
        $model = $model->where(['publicado' => 1])->orderBy($order_by[0], $order_by[1])->paginate($limit);

        return $model;
    }

    private function filtros($params)
    {
        $model = $this->model;
        if(!empty($params['busca'])){
            $model = $model->where('nome','like','%'.$params['busca'].'%');
        }
        if(!empty($params['publicado'])){
            $model = $model->where(['publicado' => $params['publicado']]);
        }
        if(!empty($params['ids'])){
            $model = $model->whereIn('id' , $params['ids']);
        }

        return $model;
    }
}