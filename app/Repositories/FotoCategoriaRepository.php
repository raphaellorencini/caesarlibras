<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class FotoCategoriaRepository extends BaseRepository
{
    private $not_in = [1,7];
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\FotoCategoria";
    }

    function listar($order_by = null, $limit = null)
    {
        if(empty($limit)) {
            if(empty($order_by)){
                $order_by = 'nome';
            }
            $model = $this->model->where(['publicado' => 1])->whereNotIn('id', $this->not_in)->orderBy($order_by)->get();
        }else{
            if(empty($order_by)){
                $order_by = 'id';
            }
            $model = $this->model->where(['publicado' => 1])->whereNotIn('id', $this->not_in)->orderBy($order_by)->limit($limit)->get();
        }
        return $model;
    }

    function listar_categoria($grupo, $order_by = null, $limit = null)
    {
        if(empty($limit)) {
            if(empty($order_by)){
                $order_by = 'nome';
            }
            $model = $this->model->where(['grupo' => $grupo, 'publicado' => 1])->whereNotIn('id', $this->not_in)->orderBy($order_by)->get();
        }else{
            if(empty($order_by)){
                $order_by = 'id';
            }
            $model = $this->model->where(['grupo' => $grupo, 'publicado' => 1])->whereNotIn('id', $this->not_in)->orderBy($order_by)->limit($limit)->get();
        }
        return $model;
    }

    function listar_categoria_id($id, $order_by = null, $limit = null)
    {
        if(empty($limit)) {
            if(empty($order_by)){
                $order_by = 'nome';
            }
            $model = $this->model->where(['id' => $id, 'publicado' => 1])->orderBy($order_by)->get();
        }else{
            if(empty($order_by)){
                $order_by = 'id';
            }
            $model = $this->model->where(['id' => $id, 'publicado' => 1])->orderBy($order_by)->limit($limit)->get();
        }
        return $model;
    }

    function listar_paginado($grupo = null, $order_by = null, $limit = null)
    {
        if(empty($order_by)){
            $order_by = ['created_at','DESC'];
        }
        if(empty($grupo)){
            $model = $this->model->where(['publicado' => 1])->whereNotIn('id', $this->not_in)->orderBy($order_by[0], $order_by[1])->paginate($limit);
        }else {
            $model = $this->model->whereIn('grupo', $grupo)->whereNotIn('id', $this->not_in)->where(['publicado' => 1])->orderBy($order_by[0], $order_by[1])->paginate($limit);
        }

        return $model;
    }
}