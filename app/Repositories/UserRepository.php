<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\User";
    }

    function listar()
    {
        $model = $this->model->where('role','admin')->orderBy('name', 'ASC')->paginate();
        return $model;
    }

    function ultimos($quantidade = 3)
    {
        return $this->model->where('role','admin')->limit($quantidade)->orderBy('id', 'DESC')->get();
    }
}