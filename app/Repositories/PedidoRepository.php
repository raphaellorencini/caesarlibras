<?php

namespace App\Repositories;
use Prettus\Repository\Eloquent\BaseRepository;

class PedidoRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Pedido";
    }

    function listar($params = null, $order_by = null, $limit = null)
    {
        $model = $this->filtros($params);

        if(empty($limit)) {
            if(empty($order_by)){
                $order_by = ['id','DESC'];
            }
            $model = $model->orderBy($order_by[0], $order_by[1])->get();
        }else{
            if(empty($order_by)){
                $order_by = ['id','DESC'];
            }
            $model = $model->orderBy($order_by[0], $order_by[1])->limit($limit)->get();
        }
        return $model;
    }

    function listar_paginado($params = null, $order_by = null, $limit = null)
    {
        if(empty($order_by)){
            $order_by = ['id','ASC'];
        }
        $model = $this->filtros($params);
        $model = $model->orderBy($order_by[0], $order_by[1])->paginate($limit);

        return $model;
    }

    function buscar_numero($numero)
    {
        $model = $this->model->where('numero',$numero)->first();

        return $model;
    }

    private function filtros($params)
    {
        $model = $this->model;

        if(!empty($params['numero'])){
            $model = $model->where('numero','like',$params['numero']);
        }
        if(!empty($params['user_id'])){
            $model = $model->where(['user_id' => $params['user_id']]);
        }

        return $model;
    }
}