<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        //Verifica se o role do usuário autenticado bate com o $role que passamos
        if(Auth::user()->role != $role) {
            return redirect('/auth/login');
        }
        return $next($request);
    }
}
