<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'password2' => 'same:password'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O campo Nome é obrigatório!',
            'email.required' => 'O campo E-mail é obrigatório!',
            'email.email' => 'E-mail inválido!',
            'email.unique' => 'Este E-mail já foi cadastrado!',
            'password.required' => 'O campo Senha é obrigatório!',
            'password.min' => 'O campo Senha precisa de no mínimo de 6 caracteres!',
            'password2.same' => 'O campo Repetir Senha tem que ser igual ao campo Senha!',
        ];
    }
}
