<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Requests\AlunoRequest;
use App\Http\Requests\UserEditPasswordRequest;
use App\Http\Controllers\Controller;
use App\Repositories\AlunoRepository;
use App\Repositories\PedidoItemRepository;
use App\Repositories\PedidoRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AlunoPedidosController extends Controller
{
    /**
     * @var PedidoRepository
     */
    private $pedidoRepository;
    /**
     * @var PedidoItemRepository
     */
    private $pedidoItemRepository;

    /**
     * AlunoPedidosController constructor.
     * @param PedidoRepository $pedidoRepository
     * @param PedidoItemRepository $pedidoItemRepository
     */
    public function __construct(PedidoRepository $pedidoRepository, PedidoItemRepository $pedidoItemRepository)
    {
        $this->pedidoRepository = $pedidoRepository;
        $this->pedidoItemRepository = $pedidoItemRepository;
    }

    public function index(Request $request)
    {
        $params = [];
        $params['user_id'] = Auth::user()->id;
        if(!empty($request->input('numero'))){
            $params['numero'] = $request->input('numero');
        }
        $pedidos = $this->pedidoRepository->listar_paginado($params);

        return view('aluno.pedidos.index', compact('pedidos'));
    }

    public function itens($numero)
    {
        $pedido = $this->pedidoRepository->buscar_numero($numero);
        $itens = $this->pedidoItemRepository->listar(['pedido_id' => $pedido->id]);

        return view('aluno.pedidos.itens', compact('pedido','itens'));
    }
}
