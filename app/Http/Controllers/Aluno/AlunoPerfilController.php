<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Requests\AlunoRequest;
use App\Http\Requests\UserEditPasswordRequest;
use App\Http\Controllers\Controller;
use App\Repositories\AlunoRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class AlunoPerfilController extends Controller
{

    /**
     * @var AlunoRepository
     */
    private $alunoRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * AlunoPerfilController constructor.
     * @param AlunoRepository $alunoRepository
     * @param UserRepository $userRepository
     */
    public function __construct(AlunoRepository $alunoRepository, UserRepository $userRepository)
    {
        $this->alunoRepository = $alunoRepository;
        $this->userRepository = $userRepository;
    }

    public function editar($id)
    {
        if(Auth::user()->id != $id){
            return redirect()->route('aluno.home');
        }

        $aluno = $this->alunoRepository->user_find($id);
        $aluno_id = $id;
        $estados = $this->estados();

        return view('aluno.perfil.editar', compact('aluno','aluno_id', 'estados'));
    }

    public function atualizar(AlunoRequest $request, $id)
    {
        $data = $request->all();

        $aluno = $this->alunoRepository->find($id);
        $user = $this->userRepository->find($aluno->user->id);
        if(Auth::user()->id != $aluno->user->id){
            return redirect()->route('aluno.home');
        }

        $aluno->update($data);
        $user->update($data['user']);

        return redirect()->route('aluno.perfil.editar',['id' => $aluno->user->id]);
    }

    public function senha_editar($id)
    {
        $aluno = $this->alunoRepository->user_find($id);
        return view('aluno.perfil.senha_editar', compact('aluno'));
    }

    public function senha_atualizar(UserEditPasswordRequest $request, $id)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['remember_token'] = str_random(60);

        $aluno = $this->alunoRepository->find($id);
        $user = $this->userRepository->find($aluno->user->id);

        $user->update($data);

        return redirect()->route('aluno.perfil.senha_editar',['id' => $aluno->user->id]);
    }

    private function estados()
    {
        return \Listas::estados('-- Estados --');
    }
}
