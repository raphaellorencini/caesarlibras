<?php

namespace App\Http\Controllers\Aluno;

use App\Capitulo;
use App\Http\Controllers\Controller;
use App\Repositories\CapituloRepository;
use App\Repositories\CursoRepository;
use App\Repositories\PedidoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AlunoCursosController extends Controller
{
    /**
     * @var PedidoRepository
     */
    private $pedidoRepository;
    /**
     * @var CursoRepository
     */
    private $cursoRepository;
    /**
     * @var CapituloRepository
     */
    private $capituloRepository;

    /**
     * AlunoCursosController constructor.
     * @param PedidoRepository $pedidoRepository
     * @param CursoRepository $cursoRepository
     * @param CapituloRepository $capituloRepository
     */
    public function __construct(PedidoRepository $pedidoRepository, CursoRepository $cursoRepository, CapituloRepository $capituloRepository)
    {

        $this->pedidoRepository = $pedidoRepository;
        $this->cursoRepository = $cursoRepository;
        $this->capituloRepository = $capituloRepository;
    }

    public function index(Request $request)
    {
        $params = [];
        $params['user_id'] = Auth::user()->id;
        if(!empty($request->input('numero'))){
            $params['numero'] = $request->input('numero');
        }
        $cursos_ids = [];
        $pedidos = $this->pedidoRepository->listar($params);
        foreach ($pedidos as $v){
            $itens = $v->itens;
            foreach ($itens as $v2){
                $cursos_ids[] = $v2->curso_id;
            }
        }

        $cursos = $this->cursoRepository->listar_paginado(['ids' => $cursos_ids]);


        return view('aluno.cursos.index', compact('pedidos', 'cursos'));
    }

    public function aulas($curso_id)
    {
        $params = [];
        $params['user_id'] = Auth::user()->id;

        $cursos_qtd = 0;
        $pedidos = $this->pedidoRepository->listar($params);
        foreach ($pedidos as $v){
            $itens = $v->itens()->where('curso_id', $curso_id)->count();
            $cursos_qtd += $itens;
        }
        if($cursos_qtd == 0){
            return redirect()->route('aluno.cursos');
        }

        $curso = $this->cursoRepository->find($curso_id);
        $capitulos_listar = $this->capituloRepository->listar(['curso_id' => $curso_id]);
        $capitulos = [];
        $i = 0;
        foreach ($capitulos_listar as $v){
            $capitulos[$i] = $v;
            $capitulos[$i]['aulas'] = $v->aulas;
            $i++;
        }


        return view('aluno.cursos.aulas', compact('capitulos', 'curso'));
    }
}
