<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AulaRequest;
use App\Repositories\CursoRepository;
use App\Repositories\CapituloRepository;
use App\Repositories\AulaRepository;

class AdminAulasController extends Controller
{
    /**
     * @var CursoRepository
     */
    private $cursoRepository;
    /**
     * @var CapituloRepository
     */
    private $capituloRepository;
    /**
     * @var AulaRepository
     */
    private $aulaRepository;

    /**
     * AdminAulasController constructor.
     * @param CapituloRepository $capituloRepository
     * @param AulaRepository $aulaRepository
     */
    public function __construct(CursoRepository $cursoRepository, CapituloRepository $capituloRepository, AulaRepository $aulaRepository)
    {
        $this->cursoRepository = $cursoRepository;
        $this->capituloRepository = $capituloRepository;
        $this->aulaRepository = $aulaRepository;
    }

    public function index($capitulo_id)
    {
        $capitulo  = $this->capituloRepository->find($capitulo_id);
        $curso = $this->cursoRepository->find($capitulo->curso_id);
        $params = ['capitulo_id' => $capitulo_id];
        $aulas = $this->aulaRepository->listar_paginado($params);

        return view('admin.aulas.index', compact('aulas','capitulo','curso','capitulo_id'));
    }

    public function novo($capitulo_id)
    {
        $capitulo  = $this->capituloRepository->find($capitulo_id);
        return view('admin.aulas.novo',compact('capitulo','capitulo_id'));
    }

    public function salvar(AulaRequest $request, $capitulo_id)
    {
        $data = $request->all();
        $data['capitulo_id'] = $capitulo_id;

        $this->aulaRepository->create($data);

        return redirect()->route('admin.aulas.index',['capitulo_id' => $capitulo_id]);
    }

    public function editar($id)
    {
        $aula = $this->aulaRepository->find($id);
        $capitulo_id = $aula->capitulo_id;
        $capitulo  = $this->capituloRepository->find($capitulo_id);
        return view('admin.aulas.editar', compact('aula','capitulo','capitulo_id'));
    }

    public function atualizar(AulaRequest $request, $id)
    {
        $data = $request->all();
        $aula = $this->aulaRepository->find($id);
        $aula->update($data);
        $capitulo_id = $aula->capitulo_id;

        return redirect()->route('admin.aulas.index',['capitulo_id' => $capitulo_id]);
    }

    public function delete($id)
    {
        $aula = $this->aulaRepository->find($id);
        $capitulo_id = $aula->capitulo_id;
        $aula->delete();

        return redirect()->route('admin.aulas.index',['capitulo_id' => $capitulo_id]);
    }

}
