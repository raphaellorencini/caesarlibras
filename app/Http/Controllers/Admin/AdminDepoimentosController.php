<?php

namespace App\Http\Controllers\Admin;

use App\Depoimento;
use App\Http\Controllers\Controller;
use App\Http\Requests\DepoimentoRequest;
use App\Repositories\DepoimentoRepository;


class AdminDepoimentosController extends Controller
{

    /**
     * @var DepoimentoRepository
     */
    private $depoimentoRepository;

    public function __construct(DepoimentoRepository $depoimentoRepository)
    {
        $this->depoimentoRepository = $depoimentoRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $depoimentos = $this->depoimentoRepository->listar();
        return view('admin.depoimentos.index', compact('depoimentos'));
    }

    public function novo()
    {
        return view('admin.depoimentos.novo');
    }

    public function salvar(DepoimentoRequest $request)
    {

        $data = $request->all();
        Depoimento::create($data);
        return redirect()->route('admin.depoimentos.index');
    }

    public function editar($id)
    {
        $depoimento = Depoimento::find($id);
        return view('admin.depoimentos.editar', compact('depoimento'));
    }

    public function atualizar(DepoimentoRequest $request, $id)
    {
        $data = $request->all();
        Depoimento::find($id)->update($data);

        return redirect()->route('admin.depoimentos.index');
    }

    public function delete($id)
    {
        Depoimento::find($id)->delete();

        return redirect()->route('admin.depoimentos.index');
    }
}
