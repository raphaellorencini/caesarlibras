<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CursoRequest;
use App\Http\Controllers\Controller;
use App\Repositories\CursoRepository;
use Illuminate\Http\Request;

class AdminCursosController extends Controller
{

    /**
     * @var CursoRepository
     */
    private $cursoRepository;

    public function __construct(CursoRepository $cursoRepository)
    {
        $this->cursoRepository = $cursoRepository;
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = [];
        if(!empty($request->get('busca'))){
            $params['busca'] = $request->get('busca');
        }
        if(!empty($request->get('publicado'))){
            $params['publicado'] = $request->get('publicado');
        }
        $cursos = $this->cursoRepository->listar_paginado($params);
        return view('admin.cursos.index', compact('cursos','params'));
    }

    public function novo()
    {
        return view('admin.cursos.novo');
    }

    public function salvar(CursoRequest $request)
    {
        $data = $request->all();
        $data['preco'] = \Formata::currency_db($data['preco']);

        $this->cursoRepository->create($data);

        return redirect()->route('admin.cursos.index');
    }


    public function editar($id)
    {
        $curso = $this->cursoRepository->find($id);
        $curso_id = $id;

        return view('admin.cursos.editar', compact('curso','curso_id'));
    }

    public function atualizar(CursoRequest $request, $id)
    {
        $data = $request->all();
        $data['preco'] = \Formata::currency_db($data['preco']);
        $curso = $this->cursoRepository->find($id);
        $curso->update($data);

        return redirect()->route('admin.cursos.index');
    }

    public function delete($id)
    {
        $this->cursoRepository->find($id)->delete();

        return redirect()->route('admin.cursos.index');
    }
}
