<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NoticiaRequest;
use App\Noticia;
use App\Http\Controllers\Controller;

class AdminNoticiasController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Noticia::orderBy('created_at','DESC')->paginate();
        return view('admin.noticias.index', compact('noticias'));
    }

    public function novo()
    {
        return view('admin.noticias.novo');
    }

    public function salvar(NoticiaRequest $request)
    {
        $data = $request->all();
        Noticia::create($data);
        return redirect()->route('admin.noticias.index');
    }

    public function editar($id)
    {
        $noticia = Noticia::find($id);
        return view('admin.noticias.editar', compact('noticia'));
    }

    public function atualizar(NoticiaRequest $request, $id)
    {
        $data = $request->all();
        Noticia::find($id)->update($data);

        return redirect()->route('admin.noticias.index');
    }

    public function delete($id)
    {
        Noticia::find($id)->delete();

        return redirect()->route('admin.noticias.index');
    }
}
