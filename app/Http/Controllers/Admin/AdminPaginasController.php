<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PaginaRequest;
use App\Pagina;
use App\Http\Controllers\Controller;
use App\PaginaTexto;
use Illuminate\Http\Request;

class AdminPaginasController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginas = Pagina::paginate();
        return view('admin.paginas.index', compact('paginas'));
    }

    public function novo()
    {
        $textos_json = '{id: "", pagina_id: "", titulo: "", texto: ""}';
        $pagina_id = null;

        return view('admin.paginas.novo', compact('textos_json','pagina_id'));
    }

    public function salvar(Request $request)
    {
        $data = $request->all();
        Pagina::create($data);

        return redirect()->route('admin.paginas.index');
    }


    public function editar($id)
    {
        $pagina = Pagina::find($id);
        $pagina_id = $id;

        return view('admin.paginas.editar', compact('pagina','pagina_id'));
    }

    public function atualizar(Request $request, $id)
    {
        $data = $request->all();
        $pagina = Pagina::find($id);
        $pagina->update($data);

        return redirect()->route('admin.paginas.index');
    }

    public function delete($id)
    {
        Pagina::find($id)->delete();

        return redirect()->route('admin.paginas.index');
    }
}
