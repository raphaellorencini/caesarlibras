<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CapituloRequest;
use App\Repositories\CursoRepository;
use App\Repositories\CapituloRepository;

class AdminCapitulosController extends Controller
{
    /**
     * @var CursoRepository
     */
    private $cursoRepository;
    /**
     * @var CapituloRepository
     */
    private $capituloRepository;

    /**
     * AdminCapitulosController constructor.
     * @param CursoRepository $cursoRepository
     * @param CapituloRepository $capituloRepository
     */
    public function __construct(CursoRepository $cursoRepository, CapituloRepository $capituloRepository)
    {
        $this->cursoRepository = $cursoRepository;
        $this->capituloRepository = $capituloRepository;
    }

    public function index($curso_id)
    {
        $curso  = $this->cursoRepository->find($curso_id);

        $params = ['curso_id' => $curso_id];
        $capitulos = $this->capituloRepository->listar_paginado($params);

        return view('admin.capitulos.index', compact('capitulos','curso','curso_id'));
    }

    public function novo($curso_id)
    {
        $curso  = $this->cursoRepository->find($curso_id);
        return view('admin.capitulos.novo',compact('curso','curso_id'));
    }

    public function salvar(CapituloRequest $request, $curso_id)
    {
        $data = $request->all();
        $data['curso_id'] = $curso_id;

        $this->capituloRepository->create($data);

        return redirect()->route('admin.capitulos.index',['curso_id' => $curso_id]);
    }

    public function editar($id)
    {
        $capitulo = $this->capituloRepository->find($id);
        $curso_id = $capitulo->curso_id;
        $curso  = $this->cursoRepository->find($curso_id);
        return view('admin.capitulos.editar', compact('capitulo','curso','curso_id'));
    }

    public function atualizar(CapituloRequest $request, $id)
    {
        $data = $request->all();
        $capitulo = $this->capituloRepository->find($id);
        $capitulo->update($data);
        $curso_id = $capitulo->curso_id;

        return redirect()->route('admin.capitulos.index',['curso_id' => $curso_id]);
    }

    public function delete($id)
    {
        $capitulo = $this->capituloRepository->find($id);
        $curso_id = $capitulo->curso_id;
        $capitulo->delete();

        return redirect()->route('admin.capitulos.index',['curso_id' => $curso_id]);
    }

}
