<?php

namespace App\Http\Controllers;

use App\Repositories\CursoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagseguroController extends Controller
{
    /**
     * @var CursoRepository
     */
    private $cursoRepository;

    /**
     * Create a new controller instance.
     *
     * @param CursoRepository $cursoRepository
     */
    public function __construct(CursoRepository $cursoRepository)
    {

        $this->cursoRepository = $cursoRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teste = true;
        if($teste !== false){
            $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout';
            $token = '80BEC0741A454C81A19A61E5FD8F50F2';
        }else{
            $url = 'https://ws.pagseguro.uol.com.br/v2/checkout';
            $token = '7ED1FBE562F648CEBB57DDB937719FA7';
        }
        $data['token'] = $token;
        $data['email'] = 'raphaellorencini@gmail.com';
        $data['currency'] = 'BRL';

        $i = 1;
        foreach (session('carrinho') as $v) {
            $v = (Object)$v;
            $data['itemId'.$i] = $v->id;
            $data['itemQuantity'.$i] = '1';
            $data['itemDescription'.$i] = $v->nome;
            $data['itemAmount'.$i] = sprintf("%.2f",$v->preco2);

            $i++;
        }

        $charset = 'UTF-8';//'ISO-8859-1';

        $data = http_build_query($data);

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: application/x-www-form-urlencoded; charset=" . $charset]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $xml= curl_exec($curl);
        curl_close ($curl);
        if($xml == 'Unauthorized'){
            $return = 'Não Autorizado';
            echo $return;
            exit;
        }

        $xml= simplexml_load_string($xml);

        if(count($xml -> error) > 0){
            $return = 'Dados Inválidos '.$xml ->error-> message;
            echo $return;
            exit;
        }

        echo $xml->code;
    }

    /*Carrinho*/

    public function adicionar(Request $request)
    {
        //session()->flush();

        $data = $request->all();
        $id = $data['cod'];
        $curso = $this->cursoRepository->find($id);

        if(!empty($curso)) {
            if(!empty(session('carrinho'))){
                $session_data = session('carrinho');
                foreach ($session_data as $v){
                    $v = (Object)$v;
                    $sessao['carrinho'][$v->id] = [
                        'id' => $v->id,
                        'imagem' => $v->imagem,
                        'nome' => $v->nome,
                        'preco' => $v->preco,
                        'preco2' => $v->preco2
                    ];
                }

            }
            $sessao['carrinho'][$curso->id] = [
                'id' => $curso->id,
                'imagem' => $curso->arquivo,
                'nome' => $curso->nome,
                'preco' => \Formata::currency($curso->preco - $curso->desconto),
                'preco2' => $curso->preco - $curso->desconto
            ];

            session($sessao);
            return response()->json(['adicionado' => '1']);
        }

        return response()->json(['adicionado' => '2']);
    }

    public function remover(Request $request)
    {
        //session()->flush();

        $data = $request->all();
        $id = $data['cod'];
        $sessao = session('carrinho');
        unset($sessao[$id]);
        session(['carrinho' => $sessao]);

        return response()->json(['removido' => '1']);
    }
}
