<?php

namespace App\Http\Controllers;

use App\Repositories\CursoRepository;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use App\Repositories\BannerRepository;
use App\Repositories\ColaboradorRepository;
use App\Repositories\DepoimentoRepository;
use App\Repositories\FotoCategoriaRepository;
use App\Repositories\FotoRepository;
use App\Repositories\NoticiaRepository;
use App\Repositories\PaginaRepository;

class SiteController extends Controller
{
    /**
     * @var NoticiaRepository
     */
    private $noticiaRepository;

    /**
     * @var FotoCategoriaRepository
     */
    private $fotoCategoriaRepository;
    /**
     * @var BannerRepository
     */
    private $bannerRepository;
    /**
     * @var PaginaRepository
     */
    private $paginaRepository;
    /**
     * @var FotoRepository
     */
    private $fotoRepository;
    /**
     * @var DepoimentoRepository
     */
    private $depoimentoRepository;
    /**
     * @var ColaboradorRepository
     */
    private $colaboradorRepository;
    /**
     * @var CursoRepository
     */
    private $cursoRepository;

    /**
     * SiteController constructor.
     * @param NoticiaRepository $noticiaRepository
     * @param FotoCategoriaRepository $fotoCategoriaRepository
     * @param BannerRepository $bannerRepository
     * @param PaginaRepository $paginaRepository
     * @param FotoRepository $fotoRepository
     * @param DepoimentoRepository $depoimentoRepository
     * @param ColaboradorRepository $colaboradorRepository
     * @param CursoRepository $cursoRepository
     */
    public function __construct
    (
        NoticiaRepository $noticiaRepository,
        FotoCategoriaRepository $fotoCategoriaRepository,
        BannerRepository $bannerRepository,
        PaginaRepository $paginaRepository,
        FotoRepository $fotoRepository,
        DepoimentoRepository $depoimentoRepository,
        ColaboradorRepository $colaboradorRepository,
        CursoRepository $cursoRepository
    )
    {
        $this->noticiaRepository = $noticiaRepository;
        $this->fotoCategoriaRepository = $fotoCategoriaRepository;
        $this->bannerRepository = $bannerRepository;
        $this->paginaRepository = $paginaRepository;
        $this->fotoRepository = $fotoRepository;
        $this->depoimentoRepository = $depoimentoRepository;
        $this->colaboradorRepository = $colaboradorRepository;
        $this->cursoRepository = $cursoRepository;
    }

    public function login()
    {
        return redirect('auth/login');
    }

    public function logout()
    {
        return redirect('auth/logout');
    }

    public function index()
    {
        $equipe = $this->equipe();
        $banners = $this->bannerRepository->listar();
        $texto = $this->pagina('home');

        return view('site.index', compact('equipe','banners', 'texto'));
    }

    public function quem_somos()
    {
        $equipe = $this->equipe();
        $clientes = $this->clientes();

        $texto = $this->pagina('quem_somos');

        return view('site.quem_somos', compact('equipe','clientes','texto'));
    }

    public function cesar()
    {
        $texto = $this->pagina('cesar');
        return view('site.cesar', compact('texto'));
    }

    public function curriculo()
    {
        $texto = $this->pagina('curriculo');
        return view('site.curriculo', compact('texto'));
    }

    public function informacoes()
    {
        $texto = $this->pagina('informacoes');
        return view('site.informacoes', compact('texto'));
    }

    public function fotos()
    {
        $fotos = $this->galeria_fotos();
        return view('site.fotos', compact('fotos'));
    }

    public function fotos_visualizar($id)
    {
        $fotos = $this->galeria_fotos($id);
        return view('site.fotos_visualizar', compact('fotos'));
    }

    public function videos()
    {
        $fotos = $this->galeria_fotos(1);
        return view('site.videos', compact('fotos'));
    }

    public function noticias()
    {
        $noticias = $this->noticiaRepository->ultimos();
        $ids = [];
        foreach ($noticias as $v){
            $ids[] = $v->id;
        }
        $noticias2 = $this->noticiaRepository->outras($ids);

        return view('site.noticias',compact('noticias','noticias2'));
    }

    public function noticia($slug)
    {
        $noticia = $this->noticiaRepository->exibir_por_slug($slug);

        return view('site.noticia',compact('noticia'));
    }

    public function cultura_surdos()
    {
        $texto = $this->pagina('cultura_surdos');
        return view('site.cultura_surdos', compact('texto'));
    }

    public function sobre()
    {
        $texto = $this->pagina('sobre');
        return view('site.sobre', compact('texto'));
    }

    public function servicos()
    {
        $texto = $this->pagina('servicos');
        return view('site.servicos', compact('texto'));
    }

    public function contato()
    {
        return view('site.contato');
    }

    public function contato_enviar(Request $request)
    {
        $valores = [
            'nome' => $request->input('nome'),
            'email' => $request->input('email'),
            'telefone' => $request->input('telefone'),
            'mensagem' => $request->input('mensagem'),
        ];

        Mail::send('site.contato_enviar', $valores, function ($message)
        {
            $message->subject('Contato Site');
            //$message->bcc('raphaellorencini@gmail.com');
            $message->cc('caesarlibras@caesarlibras.com.br');
        });

        $request->session()->flash('enviado', 'Mensagem enviada com sucesso!');

        return Redirect::to(URL::previous() . "#formulario");
    }

    public function carrinho()
    {
        $teste = true;
        if($teste !== false){
            $carrinho_url = 'https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html';
        }else{
            $carrinho_url = 'https://pagseguro.uol.com.br/checkout/v2/payment.html';
        }
        return view('site.carrinho', compact('carrinho_url'));
    }

    public function cursos(Request $request)
    {
        $params = [];
        $busca = $request->get('busca');
        if(!empty($busca)){
            $params['busca'] = $busca;
        }
        $cursos = $this->cursoRepository->listar_paginado($params);

        $cursos_lista = [];
        $i = 0;
        foreach ($cursos as $v){
            $curso = $v;
            $cursos_lista[$i] = $v;

            $valor_original = null;
            $valor_final = '<span class="curso-valor">R$ '.\Formata::currency($curso->preco).'</span>';
            $valor_parcelado = '<span class="curso-parcela">R$ '.\Formata::currency($curso->preco/12).'</span>';
            if(!empty($curso->desconto) && $curso->desconto > 0){
                $valor_original = "<s class='curso-valor-original'>R\$ ".\Formata::currency($curso->preco)."</s>";
                $valor_final = '<span class="curso-valor">R$ '.\Formata::currency($curso->preco - $curso->desconto).'</span>';
                $valor_parcelado = '<span class="curso-parcela">R$ '.\Formata::currency(($curso->preco - $curso->desconto)/12).'</span>';
            }
            $cursos_lista[$i]->valor_original = $valor_original;
            $cursos_lista[$i]->valor_final = $valor_final;
            $cursos_lista[$i]->valor_parcelado = $valor_parcelado;

            $i++;
        }

        return view('site.cursos', compact('cursos','cursos_lista'));
    }

    public function curso($id)
    {
        $curso = $this->cursoRepository->find($id);
        $valor_original = null;
        $valor_final = '<span class="curso-valor">R$ '.\Formata::currency($curso->preco).'</span>';
        $valor_parcelado = '<span class="curso-parcela">R$ '.\Formata::currency($curso->preco/12).'</span>';
        if(!empty($curso->desconto) && $curso->desconto > 0){
            $valor_original = "<s class='curso-valor-original'>R\$ ".\Formata::currency($curso->preco)."</s>";
            $valor_final = '<span class="curso-valor">R$ '.\Formata::currency($curso->preco - $curso->desconto).'</span>';
            $valor_parcelado = '<span class="curso-parcela">R$ '.\Formata::currency(($curso->preco - $curso->desconto)/12).'</span>';
        }

        return view('site.curso', compact('curso', 'valor_final', 'valor_original', 'valor_parcelado'));
    }

    private function equipe()
    {
        $colaboradores = $this->colaboradorRepository->listar();
        $equipe = [];
        foreach ($colaboradores as $v){
            $equipe[] = [
                'nome' => $v->nome,
                'texto' => $v->texto,
                'imagem' => $v->arquivo,
            ];
        }

        $equipe = json_decode(json_encode($equipe), false);

        return $equipe;
    }

    private function clientes()
    {
        $clientes = $this->galeria_fotos(7);
        $val = [];
        foreach ($clientes[0]->fotos as $v){
            $val[] =[
                'imagem' => $v->arquivo,
            ];
        }
        $val = json_decode(json_encode($val), false);

        return $val;
    }

    private function pagina($slug)
    {
        $pagina = $this->paginaRepository->listar_por_slug($slug);
        $texto = [];
        foreach ($pagina->textos as $v){
            $texto[] = [
                'nome' => $v->nome,
                'texto' => $v->texto
            ];
        }

        return $texto;
    }

    /**
     * Funções
     */
    private function galeria_fotos($id = null, $limit = 10)
    {
        if(!empty($id)){
            $val_lista = $this->fotoCategoriaRepository->listar_categoria_id($id, null, $limit);
        }else {
            $val_lista = $this->fotoCategoriaRepository->listar_paginado(null, null, $limit);
        }
        $val = [];
        $i = 0;

        foreach ($val_lista as $v){
            $val[$i] = [
                'id' => $v->id,
                'nome' => $v->nome,
                'arquivo' => $v->arquivo,
                'descricao' => $v->descricao,
            ];

            foreach ($v->fotos as $v2){
                $arquivo = $v2->arquivo;
                $classe_css = "";
                if(strpos($arquivo,'youtube') !== false || strpos($arquivo,'youtu.be') !== false){
                    $arquivo = \Youtube::embed($v2->arquivo);
                    $classe_css = "fancybox.iframe";
                }
                $val[$i]['fotos'][] = [
                    'id' => $v2->id,
                    'foto_categoria_id' => $v2->foto_categoria_id,
                    'arquivo' => $arquivo,
                    'legenda' => $v2->legenda,
                    'classe_css' => $classe_css,
                ];
            }
            if(empty($val[$i]['fotos'])){
                $val[$i]['fotos'] = [];
            }
            $i++;
        }
        $val = json_decode(json_encode($val), false);

        return $val;
    }
}
