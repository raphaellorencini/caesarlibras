<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $table = 'alunos';

    protected $fillable = [
        'user_id',
        'telefone',
        'celular',
        'endereco',
        'bairro',
        'cidades',
        'estado',
        'cep',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
