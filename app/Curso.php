<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = 'cursos';

    protected $fillable = [
        'nome',
        'texto',
        'preco',
        'desconto',
        'arquivo',
        'publicado',
    ];

    public function capitulos()
    {
        return $this->hasMany(Capitulo::class);
    }
}
