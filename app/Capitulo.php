<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capitulo extends Model
{
    protected $table = 'capitulos';

    protected $fillable = [
        'curso_id',
        'nome',
        'texto',
    ];

    public function aulas()
    {
        return $this->hasMany(Aula::class, 'capitulo_id', 'id');
    }
}
