<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = [
        'numero',
        'user_id',
        'total',
        'status',
    ];

    public function itens()
    {
        return $this->hasMany(PedidoItem::class, 'pedido_id', 'id');
    }
}
