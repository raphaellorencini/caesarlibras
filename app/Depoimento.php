<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Depoimento extends Model
{
    protected $table = 'depoimentos';

    protected $fillable = [
        'nome',
        'texto',
    ];
}
