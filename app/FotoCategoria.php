<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoCategoria extends Model
{
    protected $table = 'fotos_categorias';
    protected $fillable = [
        'nome',
        'descricao',
        'arquivo',
        'grupo',
        'publicado',
    ];

    public function fotos()
    {
        return $this->hasMany(Foto::class);
    }
}
