<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*DADOS DE CONTATO PARA CARREGAR EM TODAS AS PÁGINAS*/
        $contato_texto = [];
        try {
            $pagina = DB::table('paginas')->where('slug', 'contato')->first();
            $pagina_texto = DB::table('paginas_textos')->where('pagina_id', $pagina->id)->first();
            $contato_texto = [
                'nome' => $pagina_texto->nome,
                'texto' => $pagina_texto->texto
            ];
        }catch (\Exception $e){
            $contato_texto = [
                'nome' => '',
                'texto' => ''
            ];
        }
        view()->share('contato_texto', $contato_texto);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
