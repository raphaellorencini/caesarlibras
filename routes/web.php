<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('site.index');
});
Route::get('/',['as' => 'index', 'uses' => 'SiteController@index']);

Route::group(['as' => 'site.'], function(){
    Route::get('index',['as' => 'index', 'uses' => 'SiteController@index']);
    Route::get('quem_somos',['as' => 'quem_somos', 'uses' => 'SiteController@quem_somos']);
    Route::get('cesar',['as' => 'cesar', 'uses' => 'SiteController@cesar']);
    Route::get('curriculo',['as' => 'curriculo', 'uses' => 'SiteController@curriculo']);
    Route::get('informacoes',['as' => 'informacoes', 'uses' => 'SiteController@informacoes']);
    Route::get('fotos',['as' => 'fotos', 'uses' => 'SiteController@fotos']);
    Route::get('fotos_visualizar/{id}',['as' => 'fotos_visualizar', 'uses' => 'SiteController@fotos_visualizar']);
    Route::get('videos',['as' => 'videos', 'uses' => 'SiteController@videos']);
    Route::get('noticias',['as' => 'noticias', 'uses' => 'SiteController@noticias']);
    Route::get('noticia/{slug}',['as' => 'noticia', 'uses' => 'SiteController@noticia']);
    Route::get('cultura_surdos',['as' => 'cultura_surdos', 'uses' => 'SiteController@cultura_surdos']);
    Route::get('sobre',['as' => 'sobre', 'uses' => 'SiteController@sobre']);
    Route::get('servicos',['as' => 'servicos', 'uses' => 'SiteController@servicos']);
    Route::get('contato',['as' => 'contato', 'uses' => 'SiteController@contato']);
    Route::post('contato_enviar',['as' => 'contato_enviar', 'uses' => 'SiteController@contato_enviar']);
    Route::get('cursos',['as' => 'cursos', 'uses' => 'SiteController@cursos']);
    Route::get('curso/{id}',['as' => 'curso', 'uses' => 'SiteController@curso']);

    Route::get('carrinho',['as' => 'carrinho', 'uses' => 'SiteController@carrinho']);
    Route::post('pagseguro',['as' => 'pagseguro', 'uses' => 'PagseguroController@index']);
    Route::post('pagseguro_adicionar',['as' => 'pagseguro_adicionar', 'uses' => 'PagseguroController@adicionar']);
    Route::post('pagseguro_remover',['as' => 'pagseguro_remover', 'uses' => 'PagseguroController@remover']);

    Route::get('login',['as' => 'login', 'uses' => 'SiteController@login']);
    Route::get('logout',['as' => 'logout', 'uses' => 'SiteController@logout']);
});

/*Admin*/
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['web','auth','auth.checkrole:admin']], function(){
    Route::get('/',['as' => 'index', 'uses' => 'AdminHomeController@index']);
    Route::get('home',['as' => 'home', 'uses' => 'AdminHomeController@index']);

    Route::get('banners',['as' => 'banners.index', 'uses' => 'AdminBannersController@index']);
    Route::get('banners/novo',['as' => 'banners.novo', 'uses' => 'AdminBannersController@novo']);
    Route::get('banners/editar/{id}',['as' => 'banners.editar', 'uses' => 'AdminBannersController@editar']);
    Route::post('banners/salvar',['as' => 'banners.salvar', 'uses' => 'AdminBannersController@salvar']);
    Route::post('banners/atualizar/{id}',['as' => 'banners.atualizar', 'uses' => 'AdminBannersController@atualizar']);

    Route::get('fotos_categorias',['as' => 'fotos_categorias.index', 'uses' => 'AdminFotosCategoriasController@index']);
    Route::get('fotos_categorias/novo',['as' => 'fotos_categorias.novo', 'uses' => 'AdminFotosCategoriasController@novo']);
    Route::get('fotos_categorias/editar/{id}',['as' => 'fotos_categorias.editar', 'uses' => 'AdminFotosCategoriasController@editar']);
    Route::post('fotos_categorias/salvar',['as' => 'fotos_categorias.salvar', 'uses' => 'AdminFotosCategoriasController@salvar']);
    Route::post('fotos_categorias/atualizar/{id}',['as' => 'fotos_categorias.atualizar', 'uses' => 'AdminFotosCategoriasController@atualizar']);
    Route::get('fotos_categorias/delete/{id}',['as' => 'fotos_categorias.delete', 'uses' => 'AdminFotosCategoriasController@delete']);

    Route::get('fotos/{foto_categoria_id}',['as' => 'fotos.index', 'uses' => 'AdminFotosController@index']);
    Route::get('fotos/novo/{foto_categoria_id}',['as' => 'fotos.novo', 'uses' => 'AdminFotosController@novo']);
    Route::get('fotos/novo2/{foto_categoria_id}',['as' => 'fotos.novo2', 'uses' => 'AdminFotosController@novo2']);
    Route::get('fotos/editar/{id}',['as' => 'fotos.editar', 'uses' => 'AdminFotosController@editar']);
    Route::post('fotos/salvar/{foto_categoria_id}',['as' => 'fotos.salvar', 'uses' => 'AdminFotosController@salvar']);
    Route::post('fotos/salvar2/{foto_categoria_id}',['as' => 'fotos.salvar2', 'uses' => 'AdminFotosController@salvar2']);
    Route::post('fotos/atualizar/{id}',['as' => 'fotos.atualizar', 'uses' => 'AdminFotosController@atualizar']);
    Route::get('fotos/delete/{id}',['as' => 'fotos.delete', 'uses' => 'AdminFotosController@delete']);

    Route::get('noticias',['as' => 'noticias.index', 'uses' => 'AdminNoticiasController@index']);
    Route::get('noticias/novo',['as' => 'noticias.novo', 'uses' => 'AdminNoticiasController@novo']);
    Route::get('noticias/editar/{id}',['as' => 'noticias.editar', 'uses' => 'AdminNoticiasController@editar']);
    Route::post('noticias/salvar',['as' => 'noticias.salvar', 'uses' => 'AdminNoticiasController@salvar']);
    Route::post('noticias/atualizar/{id}',['as' => 'noticias.atualizar', 'uses' => 'AdminNoticiasController@atualizar']);
    Route::get('noticias/delete/{id}',['as' => 'noticias.delete', 'uses' => 'AdminNoticiasController@delete']);

    Route::get('paginas',['as' => 'paginas.index', 'uses' => 'AdminPaginasController@index']);
    Route::get('paginas/novo',['as' => 'paginas.novo', 'uses' => 'AdminPaginasController@novo']);
    Route::get('paginas/editar/{id}',['as' => 'paginas.editar', 'uses' => 'AdminPaginasController@editar']);
    Route::post('paginas/salvar',['as' => 'paginas.salvar', 'uses' => 'AdminPaginasController@salvar']);
    Route::post('paginas/atualizar/{id}',['as' => 'paginas.atualizar', 'uses' => 'AdminPaginasController@atualizar']);
    Route::get('paginas/delete/{id}',['as' => 'paginas.delete', 'uses' => 'AdminPaginasController@delete']);

    Route::get('paginas_textos/{pagina_id}',['as' => 'paginas_textos.index', 'uses' => 'AdminPaginasTextosController@index']);
    Route::get('paginas_textos/novo/{pagina_id}',['as' => 'paginas_textos.novo', 'uses' => 'AdminPaginasTextosController@novo']);
    Route::get('paginas_textos/editar/{id}',['as' => 'paginas_textos.editar', 'uses' => 'AdminPaginasTextosController@editar']);
    Route::post('paginas_textos/salvar/{pagina_id}',['as' => 'paginas_textos.salvar', 'uses' => 'AdminPaginasTextosController@salvar']);
    Route::post('paginas_textos/atualizar/{id}',['as' => 'paginas_textos.atualizar', 'uses' => 'AdminPaginasTextosController@atualizar']);
    Route::get('paginas_textos/delete/{id}',['as' => 'paginas_textos.delete', 'uses' => 'AdminPaginasTextosController@delete']);

    Route::get('colaboradores',['as' => 'colaboradores.index', 'uses' => 'AdminColaboradoresController@index']);
    Route::get('colaboradores/novo',['as' => 'colaboradores.novo', 'uses' => 'AdminColaboradoresController@novo']);
    Route::get('colaboradores/editar/{id}',['as' => 'colaboradores.editar', 'uses' => 'AdminColaboradoresController@editar']);
    Route::post('colaboradores/salvar',['as' => 'colaboradores.salvar', 'uses' => 'AdminColaboradoresController@salvar']);
    Route::post('colaboradores/atualizar/{id}',['as' => 'colaboradores.atualizar', 'uses' => 'AdminColaboradoresController@atualizar']);
    Route::get('colaboradores/delete/{id}',['as' => 'colaboradores.delete', 'uses' => 'AdminColaboradoresController@delete']);

    Route::get('depoimentos',['as' => 'depoimentos.index', 'uses' => 'AdminDepoimentosController@index']);
    Route::get('depoimentos/novo',['as' => 'depoimentos.novo', 'uses' => 'AdminDepoimentosController@novo']);
    Route::get('depoimentos/editar/{id}',['as' => 'depoimentos.editar', 'uses' => 'AdminDepoimentosController@editar']);
    Route::post('depoimentos/salvar',['as' => 'depoimentos.salvar', 'uses' => 'AdminDepoimentosController@salvar']);
    Route::post('depoimentos/atualizar/{id}',['as' => 'depoimentos.atualizar', 'uses' => 'AdminDepoimentosController@atualizar']);
    Route::get('depoimentos/delete/{id}',['as' => 'depoimentos.delete', 'uses' => 'AdminDepoimentosController@delete']);

    Route::get('users',['as' => 'users.index', 'uses' => 'AdminUsersController@index']);
    Route::get('users/novo',['as' => 'users.novo', 'uses' => 'AdminUsersController@novo']);
    Route::get('users/editar/{id}',['as' => 'users.editar', 'uses' => 'AdminUsersController@editar']);
    Route::post('users/salvar',['as' => 'users.salvar', 'uses' => 'AdminUsersController@salvar']);
    Route::post('users/atualizar/{id}',['as' => 'users.atualizar', 'uses' => 'AdminUsersController@atualizar']);
    Route::get('users/senha_editar/{id}',['as' => 'users.senha_editar', 'uses' => 'AdminUsersController@senha_editar']);
    Route::post('users/senha_atualizar/{id}',['as' => 'users.senha_atualizar', 'uses' => 'AdminUsersController@senha_atualizar']);
    Route::get('users/delete/{id}',['as' => 'users.delete', 'uses' => 'AdminUsersController@delete']);

    Route::get('cursos',['as' => 'cursos.index', 'uses' => 'AdminCursosController@index']);
    Route::get('cursos/novo',['as' => 'cursos.novo', 'uses' => 'AdminCursosController@novo']);
    Route::get('cursos/editar/{id}',['as' => 'cursos.editar', 'uses' => 'AdminCursosController@editar']);
    Route::post('cursos/salvar',['as' => 'cursos.salvar', 'uses' => 'AdminCursosController@salvar']);
    Route::post('cursos/atualizar/{id}',['as' => 'cursos.atualizar', 'uses' => 'AdminCursosController@atualizar']);
    Route::get('cursos/delete/{id}',['as' => 'cursos.delete', 'uses' => 'AdminCursosController@delete']);

    Route::get('capitulos/{curso_id}',['as' => 'capitulos.index', 'uses' => 'AdminCapitulosController@index']);
    Route::get('capitulos/novo/{curso_id}',['as' => 'capitulos.novo', 'uses' => 'AdminCapitulosController@novo']);
    Route::get('capitulos/editar/{id}',['as' => 'capitulos.editar', 'uses' => 'AdminCapitulosController@editar']);
    Route::post('capitulos/salvar/{curso_id}',['as' => 'capitulos.salvar', 'uses' => 'AdminCapitulosController@salvar']);
    Route::post('capitulos/atualizar/{id}',['as' => 'capitulos.atualizar', 'uses' => 'AdminCapitulosController@atualizar']);
    Route::get('capitulos/delete/{id}',['as' => 'capitulos.delete', 'uses' => 'AdminCapitulosController@delete']);

    Route::get('aulas/{capitulo_id}',['as' => 'aulas.index', 'uses' => 'AdminAulasController@index']);
    Route::get('aulas/novo/{capitulo_id}',['as' => 'aulas.novo', 'uses' => 'AdminAulasController@novo']);
    Route::get('aulas/editar/{id}',['as' => 'aulas.editar', 'uses' => 'AdminAulasController@editar']);
    Route::post('aulas/salvar/{capitulo_id}',['as' => 'aulas.salvar', 'uses' => 'AdminAulasController@salvar']);
    Route::post('aulas/atualizar/{id}',['as' => 'aulas.atualizar', 'uses' => 'AdminAulasController@atualizar']);
    Route::get('aulas/delete/{id}',['as' => 'aulas.delete', 'uses' => 'AdminAulasController@delete']);
});

Route::group(['prefix' => 'aluno', 'as' => 'aluno.', 'namespace' => 'Aluno', 'middleware' => ['web','auth','auth.checkrole:aluno']], function(){
    Route::get('/',['as' => 'index', 'uses' => 'AlunoHomeController@index']);
    Route::get('home',['as' => 'home', 'uses' => 'AlunoHomeController@index']);

    Route::get('perfil/index',['as' => 'perfil.index', 'uses' => 'AlunoPerfilController@index']);
    Route::get('perfil/editar/{id}',['as' => 'perfil.editar', 'uses' => 'AlunoPerfilController@editar']);
    Route::post('perfil/atualizar/{id}',['as' => 'perfil.atualizar', 'uses' => 'AlunoPerfilController@atualizar']);
    Route::get('perfil/senha_editar/{id}',['as' => 'perfil.senha_editar', 'uses' => 'AlunoPerfilController@senha_editar']);
    Route::post('perfil/senha_atualizar/{id}',['as' => 'perfil.senha_atualizar', 'uses' => 'AlunoPerfilController@senha_atualizar']);

    Route::get('pedidos',['as' => 'pedidos.index', 'uses' => 'AlunoPedidosController@index']);
    Route::get('pedidos/itens/{numero}',['as' => 'pedidos.itens', 'uses' => 'AlunoPedidosController@itens']);

    Route::get('cursos',['as' => 'cursos', 'uses' => 'AlunoCursosController@index']);
    Route::get('aulas/{curso_id}',['as' => 'cursos.aulas', 'uses' => 'AlunoCursosController@aulas']);
});

/*Auth*/
Route::group(['prefix' => 'auth', 'as' => 'auth.','middleware' => ['web']], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
});

Auth::routes();

Route::get('/home', 'HomeController@index');

/*Exibe as SQL's executadas*/
#Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {var_dump($query->sql);var_dump($query->bindings);var_dump($query->time);});